//
//  Result.swift
//  Demo
//
//  Created by Ivan Zagorulko on 02/08/16.
//  Copyright © 2016 Patronus IT. All rights reserved.
//

import RxSwift

enum Result<T> {
    case Success(value: T)
    case Failure(error: Error)
}
