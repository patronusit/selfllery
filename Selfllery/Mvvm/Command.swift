//
//  Command.swift
//  Demo
//
//  Created by Ivan Zagorulko on 02/08/16.
//  Copyright © 2016 Patronus IT. All rights reserved.
//

import RxSwift

class Command {
    let canExecute: Observable<Bool>!
    private var actions: [(() -> Void)] = []
    
    init(canExecute: Observable<Bool>) {
        self.canExecute = canExecute
    }
    
    convenience init() {
        self.init(canExecute: Observable.just(true))
    }
    
    @objc
    func execute() {
        actions.forEach { action in
            action()
        }
    }
    
    func subscribe(action: @escaping () -> Void) {
        self.actions.append(action)
    }
}
