//
//  ShowPhotoViewController.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 27.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material

class ShowPhotoViewController: ViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var photoDetailScrollView: UIScrollView!
    @IBOutlet weak var photoDetailImageView: UIImageView!
    @IBOutlet weak var closeImageButton: Button!
    
    var viewModel = ShowPhotoViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoDetailScrollView.minimumZoomScale = 1.0
        photoDetailScrollView.maximumZoomScale = 6.0
        
        viewModel.closeCommand <- self.closeImageButton
        
        viewModel.closeCommand.subscribe { [unowned self] _ in
            self.dismiss(animated: true, completion: nil)
        }
        
        photoDetailImageView.image = viewModel.preseentPhoto.get
        
//        self.photoDetailImageView.layer.borderWidth = 1.0
//        self.photoDetailImageView.layer.borderColor = UIColor.black.cgColor
//        self.photoDetailImageView.layer.cornerRadius = 10.0
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photoDetailImageView
    }
}
