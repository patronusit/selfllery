//
//  AboutViewController.swift
//  Selfllery
//
//  Created by victor on 28.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import Material
import MaterialComponents

class AboutViewController: ViewController {
    
    @IBOutlet weak var menuBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var FAQButton: SLButton!
    @IBOutlet weak var privacyButton: SLButton!
    @IBOutlet weak var termsButton: SLButton!
    @IBOutlet weak var textLbl: UILabel!
    
    let viewModel = AboutViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "About"
        
        viewModel.menuCommand <- self.menuBarButtonItem
        viewModel.FAQButtonCommand <- self.FAQButton
        viewModel.privacyButtonCommand <- self.privacyButton
        viewModel.termsButtonCommand <- self.termsButton
        
        viewModel.menuCommand.subscribe { [unowned self] _ in
            if let drawerViewController = self.drawerViewController {
                drawerViewController.setDrawerState(.opened, animated: true)
            }
        }
        
        viewModel.FAQButtonCommand.subscribe {
            UIApplication.shared.open(URL(string : "https://selfllery.com/app/faq/en")!, options: [:], completionHandler: { (status) in
            })
        }
        
        viewModel.privacyButtonCommand.subscribe {
            UIApplication.shared.open(URL(string : "https://selfllery.com/app/privacy/en")!, options: [:], completionHandler: { (status) in
            })
        }
        
        viewModel.termsButtonCommand.subscribe {
            UIApplication.shared.open(URL(string : "https://selfllery.com/app/terms/en")!, options: [:], completionHandler: { (status) in
            })
        }
    }
    
}
