//
//  FogotPasswordViewController.swift
//  Selfllery
//
//  Created by victor on 19.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class FogotPasswordViewController: ViewController  {
    
    @IBAction func contactSupport(_ sender: Any) {
        UIApplication.shared.open(URL(string : "https://selfllery.com/contact")!, options: [:], completionHandler: { (status) in
        })
    }
    @IBAction func fogotPassAction(_ sender: Any) {
        UIApplication.shared.open(URL(string : "https://selfllery.com/lostpass")!, options: [:], completionHandler: { (status) in
        })
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
