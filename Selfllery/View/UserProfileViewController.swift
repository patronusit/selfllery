//
//  UserProfileViewController.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 20.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialSlider

class UserProfileViewController: ViewController {

    @IBOutlet weak var tabBarContentView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var hideUserButton: UIButton!
    
    @IBOutlet weak var profileIconImageView: UIImageView!
    @IBOutlet weak var profileBackImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userRatingLabel: UILabel!
    @IBOutlet weak var userFollowersLabel: UILabel!
    @IBOutlet weak var userFollowingLabel: UILabel!
    
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var userInfoContentView: UIView!
    @IBOutlet weak var userInfoFirstNameLabel: UILabel!
    @IBOutlet weak var userInfoLastNameLabel: UILabel!
    @IBOutlet weak var userInfoCountryLabel: UILabel!
    @IBOutlet weak var userInfoSexTitleLabel: UILabel!
    @IBOutlet weak var userInfoSexLabel: UILabel!
    @IBOutlet weak var userInfoDoBTitleLabel: UILabel!
    @IBOutlet weak var userInfoDoBLabel: UILabel!
    @IBOutlet weak var userInfoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var userInfoBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var userSettingsContentView: UIView!
    @IBOutlet weak var charityTableView: UITableView!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var charityButton: UIButton!
    @IBOutlet weak var percentSlider: UISlider!
    
    @IBOutlet weak var userReviewsContentView: UIView!
    @IBOutlet weak var reviewsTableView: UITableView!
    
    @IBOutlet weak var userFollowersContentView: UIView!
    @IBOutlet weak var userFollowersTableView: UITableView!
    
    @IBOutlet weak var userFollowingContentView: UIView!
    @IBOutlet weak var userFollowingTableView: UITableView!
    
    @IBOutlet weak var userProfileContentScrollView: UIScrollView!
    @IBOutlet weak var userReviewConstraint: NSLayoutConstraint!
   
    
    let viewModel = UserProfileViewModel()
    
    lazy var tabBarContainer: MDCTabBar = {
        
        let tabBar = MDCTabBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.tabBarContentView.bounds.height))
                
        tabBar.delegate = self
        tabBar.alignment = MDCTabBarAlignment.leading
        
        tabBar.tintColor = UIColor(hex: 0xeb6161)
        tabBar.selectedItemTintColor = UIColor(hex: 0xea5050)
        tabBar.unselectedItemTintColor = UIColor(hex: 0x969696)
        
        return tabBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTabBar()
        
        self.setupSlider()
        
        self.viewModel.backCommand <- self.backButton
        self.viewModel.charityUserCommand <- self.charityButton
        self.viewModel.userCharityPercent <- self.percentLabel
        
        if self.viewModel.isOwnProfile.get {
            self.viewModel.editProfileCommand <- self.actionButton
            self.actionButton.setTitle("EDIT", for: .normal)
            self.hideUserButton.isHidden = true
            self.userInfoBottomConstraint.priority = 250
        } else {
            self.viewModel.hideUserCommand <- self.hideUserButton
            self.hideUserButton.isHidden = false
            self.userInfoSexTitleLabel.isHidden = true
            self.userInfoSexLabel.isHidden = true
            self.userInfoDoBTitleLabel.isHidden = true
            self.userInfoDoBLabel.isHidden = true
            self.userInfoTopConstraint.priority = 750
            self.userInfoBottomConstraint.priority = 250
            self.viewModel.followUserCommand <- self.actionButton
        }
        
        self.viewModel.backCommand.subscribe { [unowned self] _ in
            if self.viewModel.isOwnProfile.get {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        self.viewModel.editProfileCommand.subscribe { [unowned self] _ in
            self.performSegue(withIdentifier: "editUserProfile", sender: self)
        }
        
        self.viewModel.followUserCommand.subscribe(onNext: { _ in
            self.viewModel.loadUserFollowersCommand.execute()
            self.updateUserInfo()
        })
        
        self.viewModel.hideUserCommand.subscribe(onNext: { [unowned self] result in
            switch result {
            case .Success(let _):
                self.hideUserButton.setTitle("HIDDEN", for: .normal)
                self.hideUserButton.isSelected = false
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
            
        })
        
        self.viewModel.currentUser.change.subscribe(onNext: { userInfo in
            if userInfo != nil {
                self.updateUserInfo()
            }
        })
        
        self.viewModel.userPhotos.change.subscribe(onNext: { _ in
            if self.viewModel.activeTabNumber.get == 0 {
                self.photoCollectionView.reloadData()
            }
        })
        
        self.viewModel.userFollowers.change.subscribe(onNext: { _ in
            if self.viewModel.activeTabNumber.get == 3 {
                self.userFollowersTableView.reloadData()
            }
        })
        
        self.viewModel.userFollowing.change.subscribe(onNext: { _ in
            if self.viewModel.activeTabNumber.get == 4 {
                self.userFollowingTableView.reloadData()
            }
        })
        
        self.viewModel.userSettings.change.subscribe(onNext: { _ in
            if self.viewModel.activeTabNumber.get == 2 {
                self.charityTableView.reloadData()
            }
        })
        
        self.viewModel.activeTabNumber.change.subscribe(onNext: { tabNumber in
            switch tabNumber {
                case 0:
                    self.photoCollectionView.reloadData()
                    break
                case 2:
                    if self.viewModel.isOwnProfile.get {
                        self.charityTableView.reloadData()
                    } else {
                        self.reviewsTableView.reloadData()
                    }
                    break
                case 3:
                    if self.viewModel.isOwnProfile.get {
                        self.reviewsTableView.reloadData()
                    } else {
                        self.userFollowersTableView.reloadData()
                    }
                    break
                case 4:
                    if self.viewModel.isOwnProfile.get {
                        self.userFollowersTableView.reloadData()
                    } else {
                        self.userFollowingTableView.reloadData()
                    }
                    break
                case 5:
                    if self.viewModel.isOwnProfile.get {
                        self.userFollowingTableView.reloadData()
                    }
                    break
                default:
                    break
            }
        })
        
        self.viewModel.charityUserCommand.subscribe(onNext: { [unowned self] result in
            switch result {
            case .Success(let value):
                var title = ""
                var message = ""
                if value {
                    title = "Success"
                    message = "Settings updated successfully"
                } else {
                    title = "Error!"
                    message = "Settings not updated"
                }
                let alert = UIAlertController.customAlertController(title: title, message: message)
                self.present(alert, animated: true) {}
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        })
        
        self.viewModel.loadUserProfileCommand.subscribe(onNext: {_ in
            self.viewModel.loadUserFollowersCommand.execute()
            self.viewModel.loadUserFollowingCommand.execute()
            self.viewModel.loadUserCommentsCommand.execute()
            if self.viewModel.isOwnProfile.get {
                self.viewModel.loadUserSettingsCommand.execute()
            }
        })
        
        self.viewModel.loadUserProfileCommand.execute()
        self.viewModel.loadUserPhotoCommand.execute()
        
        let commentCellNib = UINib(nibName: "CommentTabelCell", bundle: nil)
        reviewsTableView.register(commentCellNib, forCellReuseIdentifier: "commentCell")

        self.reviewsTableView.estimatedRowHeight = 130
        self.reviewsTableView.rowHeight = UITableViewAutomaticDimension
        
        self.profileBackImageView.image = nil
        self.profileIconImageView.image = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func percentSliderChanged(_ senderSlider: UISlider) {
        let value = Int(senderSlider.value)
        //self.percentLabel.text = "\(value)%"
        //self.viewModel.userCharityPercent.set(value: "\(value)%")
    }
    
    func setupTabBar() {
        if self.viewModel.isOwnProfile.get {
            self.tabBarContainer.items =  [
                UITabBarItem(title: "PHOTOS", image: nil, tag: 0),
                UITabBarItem(title: "INFO", image: nil, tag: 0),
                //UITabBarItem(title: "CHARITY", image: nil, tag: 0),
                UITabBarItem(title: "REVIEWS", image: nil, tag: 0),
                UITabBarItem(title: "FOLLOWERS", image: nil, tag: 0),
                UITabBarItem(title: "FOLLOWING", image: nil, tag: 0)
            ]
            // must be removed
            self.userReviewConstraint.priority = 750
        } else {
            self.tabBarContainer.items =  [
                UITabBarItem(title: "PHOTOS", image: nil, tag: 0),
                UITabBarItem(title: "INFO", image: nil, tag: 0),
                UITabBarItem(title: "REVIEWS", image: nil, tag: 0),
                UITabBarItem(title: "FOLLOWERS", image: nil, tag: 0),
                UITabBarItem(title: "FOLLOWING", image: nil, tag: 0)
            ]
            self.userReviewConstraint.priority = 750
        }
        self.tabBarContainer.selectedItem = self.tabBarContainer.items[0]
        self.tabBarContentView.addSubview(self.tabBarContainer)
    }
    
    func setupSlider() {
        self.percentSlider.minimumValue = 0
        self.percentSlider.maximumValue = 100
        
        self.percentLabel.text = "\(Int(percentSlider.value))"
    }
    
    func updateUserInfo() {
        if !self.viewModel.isOwnProfile.get {
            if self.viewModel.currentUser.get!.isFollow {
                self.actionButton.setTitle("UNFOLLOW", for: .normal)
            } else {
                self.actionButton.setTitle("FOLLOW", for: .normal)
            }
        } else {
            self.percentSlider.value = Float(self.viewModel.currentUser.get!.totalPercent)
            self.percentLabel.text = "\(self.viewModel.currentUser.get!.totalPercent)%"
            
            self.userInfoSexLabel.text = self.viewModel.currentUser.get!.getGender()
            self.userInfoDoBLabel.text = self.viewModel.currentUser.get!.userBirthDate()
        }
        
        self.userNameLabel.text = self.viewModel.currentUser.get?.fullName
        //self.userRatingLabel.text = "\(self.viewModel.currentUser.get!.rating)"
        self.userRatingLabel.isHidden = true
        self.userFollowersLabel.text = "\(self.viewModel.currentUser.get!.followersCount)"
        self.userFollowingLabel.text = "\(self.viewModel.currentUser.get!.followingCount)"
        
        self.userInfoFirstNameLabel.text = self.viewModel.currentUser.get?.firstName
        self.userInfoLastNameLabel.text = self.viewModel.currentUser.get?.lastName
        self.userInfoCountryLabel.text = self.viewModel.currentUser.get?.countryName
        
        let imageCache = PersistentAutoPurgingImageCache.default
        let avatarUrl = self.viewModel.currentUser.get!.avatarUrl
        if let linkUrlString = avatarUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.profileIconImageView.image = image
            } else {
                self.profileIconImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }
        var photoUrl: String? = nil
        photoUrl = self.viewModel.currentUser.get!.backgroundUrl
        if let linkUrlString = photoUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.profileBackImageView.image = image
            } else {
                self.profileBackImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        } else {
            self.profileBackImageView.image = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showPhotoDetail" {
            let photoDetailController = segue.destination as! PhotoDetailViewController
            photoDetailController.viewModel.photoDetail.set(value: self.viewModel.selectPhoto.get)
        }
        
        if segue.identifier == "showUserProfile" {
            let userProfileController = segue.destination as! UserProfileViewController
            userProfileController.viewModel.userProfileId.set(value: self.viewModel.selectUser.get!.userID)
        }
        
        if segue.identifier == "editUserProfile" {
            let userProfileController = segue.destination as! EditProfileViewController
            userProfileController.viewModel.isOwnProfile.set(value: self.viewModel.isOwnProfile.get)
        }
    }
}

extension UserProfileViewController: MDCTabBarDelegate {
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        var index = 0
        switch tabBar.selectedItem! {
        case tabBar.items[0]:
            index = 0
            self.userProfileContentScrollView.scrollRectToVisible(self.photoCollectionView.frame, animated: true)
            break
        case tabBar.items[1]:
            index = 1
            self.userProfileContentScrollView.scrollRectToVisible(self.userInfoContentView.frame, animated: true)
            break
        case tabBar.items[2]:
            index = 2
            //if self.viewModel.isOwnProfile.get {
            //     self.userProfileContentScrollView.scrollRectToVisible(self.userSettingsContentView.frame, animated: true)
            //} else {
                self.userProfileContentScrollView.scrollRectToVisible(self.userReviewsContentView.frame, animated: true)
            //}
            break
        case tabBar.items[3]:
            index = 3
            //if self.viewModel.isOwnProfile.get {
            //    self.userProfileContentScrollView.scrollRectToVisible(self.userReviewsContentView.frame, animated: true)
            //} else {
                self.userProfileContentScrollView.scrollRectToVisible(self.userFollowersContentView.frame, animated: true)
            //}
            break
        case tabBar.items[4]:
            index = 4
            //if self.viewModel.isOwnProfile.get {
            //    self.userProfileContentScrollView.scrollRectToVisible(self.userFollowersContentView.frame, animated: true)
            //} else {
                self.userProfileContentScrollView.scrollRectToVisible(self.userFollowingContentView.frame, animated: true)
            //}
            break
//        case tabBar.items[5]:
//            if self.viewModel.isOwnProfile.get {
//                index = 5
//                self.userProfileContentScrollView.scrollRectToVisible(self.userFollowingContentView.frame, animated: true)
//            }
//            break
        default:
            break
        }
        self.viewModel.activeTabNumber.set(value: index)
    }
}

extension UserProfileViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.photoCollectionView {
            if (scrollView.bounds.maxY == scrollView.contentSize.height) {
                self.viewModel.loadGaleryPage.set(value: self.viewModel.loadGaleryPage.get + 1)
                self.viewModel.loadUserPhotoCommand.execute()
            }
        }
        if scrollView == self.userFollowersTableView {
            if (scrollView.bounds.maxY == scrollView.contentSize.height) {
                if self.viewModel.userFollowers.get.count % 24 == 0 {
                    self.viewModel.loadFollowersPage.set(value: self.viewModel.loadFollowersPage.get + 1)
                    self.viewModel.loadUserFollowersCommand.execute()
                }
            }
        }
        if scrollView == self.userFollowingTableView {
            if (scrollView.bounds.maxY == scrollView.contentSize.height) {
                if self.viewModel.userFollowing.get.count % 24 == 0 {
                    self.viewModel.loadFollowingPage.set(value: self.viewModel.loadFollowingPage.get + 1)
                    self.viewModel.loadUserFollowingCommand.execute()
                }
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == self.userProfileContentScrollView {
            let fractionalPage = scrollView.contentOffset.x / scrollView.frame.width
            let pageTab = lround(Double(fractionalPage))
            if self.tabBarContainer.items.index(of: self.tabBarContainer.selectedItem!) != pageTab {
                self.viewModel.activeTabNumber.set(value: pageTab)
                self.tabBarContainer.setSelectedItem(self.tabBarContainer.items[pageTab], animated: true)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.userProfileContentScrollView {
            let fractionalPage = scrollView.contentOffset.x / scrollView.frame.width
            let pageTab = lround(Double(fractionalPage))
            if self.tabBarContainer.items.index(of: self.tabBarContainer.selectedItem!) != pageTab {
                self.viewModel.activeTabNumber.set(value: pageTab)
                self.tabBarContainer.setSelectedItem(self.tabBarContainer.items[pageTab], animated: true)
            }
        }
    }
}

extension UserProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.userPhotos.get.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = self.viewModel.userPhotos.get[indexPath.row]
        self.viewModel.selectPhoto.set(value: photo)
        self.performSegue(withIdentifier: "showPhotoDetail", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let usableSpace = collectionView.frame.width - 2 * collectionViewContentInset - 2 * collectionViewCellContentInset
        let cellWidth: CGFloat = usableSpace / 3.0
        let cellHeight: CGFloat = usableSpace / 3.0
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCollectionCell
        let photo = self.viewModel.userPhotos.get[indexPath.row]
        cell.setSmallPhotoImage(photoUrl: photo.photoUrl)
        return cell
    }
}

extension UserProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case self.userFollowersTableView:
            return self.viewModel.userFollowers.get.count
        case self.userFollowingTableView:
            return self.viewModel.userFollowing.get.count
        case self.reviewsTableView:
            return self.viewModel.userComments.get.count
        case self.charityTableView:
            return self.viewModel.userSettings.get.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var userInfo: SLUser? = nil
        //var cell: UITableViewCell

        if tableView == self.userFollowersTableView || tableView == self.userFollowingTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "userInfoCell", for: indexPath) as! UserTableCell
            if tableView == self.userFollowersTableView {
                userInfo = self.viewModel.userFollowers.get[indexPath.row]
                cell.setUser(userInfo: userInfo!)
                return cell
            } else {
                userInfo = self.viewModel.userFollowing.get[indexPath.row]
                cell.setUser(userInfo: userInfo!)
                return cell
            }
        } else if tableView == self.reviewsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTabelCell
            cell.setComment(commentInfo: self.viewModel.userComments.get[indexPath.row])
            cell.delegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "userSettingSell", for: indexPath) as! UserSettingTabelCell
            cell.setUserSetting(settingInfo: self.viewModel.userSettings.get[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var userInfo: SLUser? = nil
        if tableView == self.userFollowersTableView {
            userInfo = self.viewModel.userFollowers.get[indexPath.row]
            self.viewModel.selectUser.set(value: userInfo)
            self.performSegue(withIdentifier: "showUserProfile", sender: self)
        }
        
        if tableView == self.userFollowingTableView {
            userInfo = self.viewModel.userFollowing.get[indexPath.row]
            self.viewModel.selectUser.set(value: userInfo)
            self.performSegue(withIdentifier: "showUserProfile", sender: self)
        }
        
        if tableView == self.charityTableView {
            let cell = tableView.cellForRow(at: indexPath) as! UserSettingTabelCell
            cell.setCheck()
            self.viewModel.changeSetting(setting: self.viewModel.userSettings.get[indexPath.row])
        }
    }
}

extension UserProfileViewController: CommentTabelCellDelegate {
    
    func complaintButtonCliced(cell: CommentTabelCell) {
    }

    func likeButtonCliced(cell: CommentTabelCell) {
    }
    
    func dislikeButtonCliced(cell: CommentTabelCell) {
    }
    
    func replyButtonCliced(cell: CommentTabelCell) {
    }
}
