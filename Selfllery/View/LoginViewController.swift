//
//  LoginViewController.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 05.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material
import MaterialComponents

import GoogleSignIn

class LoginViewController: ViewController, GIDSignInUIDelegate, GIDSignInDelegate  {
    
    @IBOutlet weak var emailTextField: SLTextField!
    @IBOutlet weak var passTextField: SLTextField!
    @IBOutlet weak var repassTextField: SLTextField!
    @IBOutlet weak var loginButton: SLButton!
    @IBOutlet weak var fbButton: SLButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var fogotPassButton: Button!
    @IBOutlet weak var googleAuthButton: SLButton!
    
    @IBOutlet weak var privacyLbl: UILabel!
    @IBOutlet weak var privacyButton: Button!
    @IBOutlet weak var termsButton: Button!
    
    let viewModel = LoginViewModel()
    let tabBar = MDCTabBar()
    
    var isAuthAction:Bool = true
    
    @IBAction func googleAuthAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func fogotPassButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: "fogot", sender: self)
    }
    
    @IBAction func logIn(_ sender: Any) {
        tabBar.selectedItem == tabBar.items[0] ? viewModel.authenticateCommand.execute() : viewModel.registerCommand.execute()
    }
    
    lazy var tabBarContainer: MDCTabBar = {
        self.tabBar.items = [
            UITabBarItem(title: "LOG IN", image: nil, tag: 0),
            UITabBarItem(title: "REGISTRATION", image: nil, tag: 0),
        ]
        
        self.tabBar.selectedItem = self.tabBar.items[0]
        self.tabBar.delegate = self
        self.tabBar.alignment = MDCTabBarAlignment.justified
        
        self.tabBar.tintColor = UIColor.green
        self.tabBar.inkColor = UIColor.clear
        self.tabBar.tintColor = UIColor(red: 235.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        self.tabBar.selectedItemTintColor = UIColor(red: 234.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        self.tabBar.unselectedItemTintColor = UIColor(red: 118.0/255.0, green: 118.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        
        return self.tabBar
    }()
    
    func setupView() {
        self.scrollView.addSubview(tabBarContainer)
        self.tabBarContainer.translatesAutoresizingMaskIntoConstraints = false
        let tabBarViews = ["tabBar": self.tabBarContainer]
        let widthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[tabBar]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: tabBarViews)
        let heightConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-111-[tabBar(60)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: tabBarViews)
        let horizontalConstraint = NSLayoutConstraint(item: self.tabBarContainer,
                                                      attribute: NSLayoutAttribute.centerX,
                                                      relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: self.tabBarContainer,
                                                    attribute: NSLayoutAttribute.centerY,
                                                    relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        view.addConstraints(widthConstraints)
        view.addConstraints(heightConstraints)
        view.addConstraints([horizontalConstraint, verticalConstraint])
        self.repassTextField.isHidden = true
        self.fogotPassButton.isHidden = false
        privacyLbl.text = "By clicking Log In you agree with our "
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error ?? "google error")
            return
        }
        self.viewModel.googleToken.set(value: user.authentication.idToken)
        self.viewModel.googleButtonCommand.execute()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        self.setupView()
        self.setupUI()
        
        viewModel.email <-> emailTextField
        viewModel.password <-> passTextField
        viewModel.repassord <-> repassTextField
        viewModel.authenticateByFBCommand <- fbButton
        
        viewModel.privacyButtonCommand <- self.privacyButton
        viewModel.termsButtonCommand <- self.termsButton
        
        emailTextField.delegate = self
        passTextField.delegate = self
        repassTextField.delegate = self
        
        viewModel.authenticateCommand.before {
            self.userInteractionEnabled(status: false)
        }
    
        viewModel.authenticateCommand.subscribe { [unowned self] result in
            self.userInteractionEnabled(status: true)
            switch result {
            case .Success(value: ):
                self.performSegue(withIdentifier: "homeSegue", sender: self)
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        }
        
        viewModel.authenticateByFBCommand.subscribe { [unowned self] result in
            switch result {
            case .Success(value: ):
                self.performSegue(withIdentifier: "homeSegue", sender: self)
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        }
        
        viewModel.googleButtonCommand.subscribe { [unowned self] result in
            switch result {
            case .Success(value: ):
                self.performSegue(withIdentifier: "homeSegue", sender: self)
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        }
        viewModel.registerCommand.before {
            self.userInteractionEnabled(status: false)
        }
        viewModel.registerCommand.subscribe { [unowned self] result in
            self.userInteractionEnabled(status: true)
            switch result {
            case .Success(value: ):
                self.performSegue(withIdentifier: "homeSegue", sender: self)
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        }
        
        viewModel.privacyButtonCommand.subscribe {
            UIApplication.shared.open(URL(string : "https://selfllery.com/app/privacy/en")!, options: [:], completionHandler: { (status) in
            })
        }
        
        viewModel.termsButtonCommand.subscribe {
            UIApplication.shared.open(URL(string : "https://selfllery.com/app/terms/en")!, options: [:], completionHandler: { (status) in
            })
        }
        
        viewModel.validUserNameCommand.subscribe { [unowned self] result in
            switch result {
            case .Success(let value):
                if value {
                    self.performSegue(withIdentifier: "homeSegue", sender: self)
                }
            case .Failure(let error):
                break
//                switch error {
//                case BackendError.Error(message: let message):
//                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
//                    self.present(alert, animated: true) {}
//                default:
//                    break
//                }
            }
        }
        
        self.scrollView.startListeningForKeyboardNotification()
        self.viewModel.validUserNameCommand.execute()
    }
    
    func userInteractionEnabled(status:Bool) {
        self.view.isUserInteractionEnabled = status
    }
    
    func setupUI() {
        self.emailTextField.prepareForLoginView()
        self.passTextField.prepareForLoginView()
        self.repassTextField.prepareForLoginView()
        self.loginButton.prepareForLoginView()
        self.fbButton.prepareFBButton()
        self.googleAuthButton.prepareGoogleButton()
    }
    
    @IBAction func prepareForUnwind(_ segue: UIStoryboardSegue) {
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.authenticateByFBCommand <- self.fbButton
        //viewModel.googleButtonCommand <- self.googleAuthButton
    }
}

// MARK: UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if isAuthAction {
            if textField == self.emailTextField {
                return !self.passTextField.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
                self.viewModel.authenticateCommand.execute()
            }
             return false
        } else {
            if textField == self.emailTextField {
                return !self.passTextField.becomeFirstResponder()
            } else if textField == self.passTextField {
                return !self.repassTextField.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
                self.viewModel.registerCommand.execute()
            }
            return false
        }
    }
}

// MARK: MDCTabBarDelegate
extension LoginViewController: MDCTabBarDelegate {
    
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
       
        if (tabBar.selectedItem == tabBar.items[0]) {
            self.loginButton.title = "LOG IN"
            isAuthAction = true
            self.repassTextField.isHidden = true
            self.fogotPassButton.isHidden = false
            privacyLbl.text = "By clicking the button “Log in” I agree with "
        } else {
            self.loginButton.title = "REGISTRATION"
            isAuthAction = false
            self.repassTextField.isHidden = false
            self.fogotPassButton.isHidden = true//By clicking Sing Up you agree with our Privacy Policy
            privacyLbl.text = "By clicking the button “Register” I agree with "
        }
        refreshContent()
    }
}

// MARK: Data
extension LoginViewController {
    func refreshContent() {
        self.emailTextField.text = ""
        self.passTextField.text = ""
        self.repassTextField.text = ""
    }
}

