//
//  PhotoDetailViewController.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 19.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import MaterialComponents
import SwiftyJSON

class PhotoDetailViewController: ViewController {
    
    @IBOutlet weak var tabBarContentView: UIView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var profileIconImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var photoDescriptionLabel: UILabel!
    @IBOutlet weak var showProfileButton: UIButton!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var complaintButton: UIButton!
    @IBOutlet weak var fullPhotoContentView: UIView!
    @IBOutlet weak var fullPhotoImageView: UIImageView!
    
    @IBOutlet weak var infoPhotoContentView: UIView!
    @IBOutlet weak var infoDescriptionLabel: UILabel!
    @IBOutlet weak var infoTagsLabel: UILabel!
    @IBOutlet weak var infoCountViewsLabel: UILabel!
    @IBOutlet weak var infoPhotoIdLabel: UILabel!
    @IBOutlet weak var infoDateLabel: UILabel!
    @IBOutlet weak var infoCategoryLabel: UILabel!
    @IBOutlet weak var infoCopyrightLabel: UILabel!
    @IBOutlet weak var infoTagsConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var commentsPhotoContentView: UIView!
    @IBOutlet weak var commentsPhotoTableView: UITableView!
    
    @IBOutlet weak var photoDetailContentScrollView: UIScrollView!
    
    var alertController: UIAlertController?
    
    let viewModel = PhotoDetailViewModel()
    
    lazy var tabBarContainer: MDCTabBar = {
        
        let tabBar = MDCTabBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.tabBarContentView.bounds.height))
        
        tabBar.items = [
            UITabBarItem(title: "PHOTO", image: nil, tag: 0),
            UITabBarItem(title: "INFO", image: nil, tag: 0),
            UITabBarItem(title: "COMMENTS", image: nil, tag: 0)
        ]
        
        tabBar.selectedItem = tabBar.items[0]
        tabBar.delegate = self
        tabBar.alignment = MDCTabBarAlignment.leading
        
        tabBar.tintColor = UIColor(hex: 0xeb6161)
        tabBar.selectedItemTintColor = UIColor(hex: 0xea5050)
        tabBar.unselectedItemTintColor = UIColor(hex: 0x969696)
        
        return tabBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarContentView.addSubview(self.tabBarContainer)
        
        self.viewModel.backCommand <- self.backButton
        self.viewModel.likePhotoCommand <- self.likeButton
        self.viewModel.showUserProfileCommand <- self.showProfileButton
        self.checkComplaintButtonStatus()
        
        self.viewModel.backCommand.subscribe { [unowned self] _ in
            self.navigationController?.popViewController(animated: true)
        }
        
        self.viewModel.showUserProfileCommand.subscribe { [unowned self] _ in
            self.performSegue(withIdentifier: "showUserProfile", sender: self)
        }
        
        self.viewModel.photoComments.change.subscribe { [unowned self] _ in
            self.commentsPhotoTableView.reloadData()
        }
        
        self.viewModel.likePhotoCommand.subscribe { [unowned self] result in
            switch result {
            case .Success(let value):
                if value {
                    self.updatePhotoInfo()
                }
            case .Failure(let error):
                print(error)
            }
        }
        
        self.viewModel.complaintPhotoCommand.subscribe { [unowned self] result in
            switch result {
            case .Success(let value):
                let res = JSON(value)
                let alert = UIAlertController.customAlertController(title: "Success", message: res["message"].stringValue)
                self.present(alert, animated: true) {}
                
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        }
        
        self.viewModel.complaintCommentsCommand.subscribe { [unowned self] result in
            switch result {
            case .Success(let value):
                let res = JSON(value)
                let alert = UIAlertController.customAlertController(title: "Success", message: res["message"].stringValue)
                self.present(alert, animated: true) {}

            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        }
        
        self.updateUserInfo()
        self.updatePhotoInfo()
        self.viewModel.loadPhotoCommentsCommand.execute()
        
        let commentCellNib = UINib(nibName: "CommentTabelCell", bundle: nil)
        commentsPhotoTableView.register(commentCellNib, forCellReuseIdentifier: "commentCell")
        
        self.commentsPhotoTableView.estimatedRowHeight = 130
        self.commentsPhotoTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func complaintButtonAction(_ sender: Any) {
        self.alertController = UIAlertController(title: "Your complaint", message: "", preferredStyle: .alert)
        self.alertController?.addTextField { textField in
            textField.placeholder = ""
            textField.isSecureTextEntry = false
            textField.textAlignment = .left
        }
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            self.viewModel.complaintPhotoText.set(value:(self.alertController?.textFields?.last?.text)! )
            self.viewModel.complaintPhotoId.set(value:  self.viewModel.localPhotoId.get)
            self.viewModel.complaintPhotoCommand.execute()
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            
        }
        self.alertController?.addAction(OKAction)
        self.alertController?.addAction(CancelAction)
        present( self.alertController!, animated: true, completion: nil)
    }
    
    @IBAction func showPhotoAction(_ sender: Any) {
        self.performSegue(withIdentifier: "showPhotoSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showUserProfile" {
            let userProfileController = segue.destination as! UserProfileViewController
            userProfileController.viewModel.userProfileId.set(value: self.viewModel.localPhoto.get!.authorId)
        }
        
        if segue.identifier == "showPhotoSegue" {
            let photoDetailController = segue.destination as! ShowPhotoViewController
            photoDetailController.viewModel.preseentPhoto.set(value: self.fullPhotoImageView.image)
        }
    }
    
    func updateUserInfo() {
        self.userNameLabel.text = self.viewModel.localPhoto.get?.photoAuthorFullName
        self.photoDescriptionLabel.text = self.viewModel.localPhoto.get?.photoTitle
        let avatarUrl = self.viewModel.localPhoto.get!.photoAuthorAvatarUrl
        let imageCache = PersistentAutoPurgingImageCache.default
        if let linkUrlString = avatarUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.profileIconImageView.image = image
            } else {
                self.profileIconImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }
        var photoUrl: String? = nil
        photoUrl = self.viewModel.localPhoto.get!.photoUrl! + ".jpg"
        if let linkUrlString = photoUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.fullPhotoImageView.image = image
            } else {
                self.fullPhotoImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }

    }
    
    func updatePhotoInfo() {
        self.likeButton.setTitle("\(self.viewModel.localPhoto.get!.countLikes)", for: .normal)
        self.viewModel.isLikedPhoto.set(value: self.viewModel.localPhoto.get!.isLiked)
        self.likeButton.isSelected = self.viewModel.isLikedPhoto.get
        if self.viewModel.localPhoto.get?.photoDescr != "" {
            self.infoDescriptionLabel.text = self.viewModel.localPhoto.get?.photoDescr
        }
        self.infoCountViewsLabel.text = "\(self.viewModel.localPhoto.get!.countView)"
        self.infoPhotoIdLabel.text = "\(self.viewModel.localPhoto.get!.photoID)"
        self.infoDateLabel.text = self.viewModel.localPhoto.get?.photoDateString()
        if let categoryTitle = SLRepository.sharedRepository.getCategory(byId: (self.viewModel.localPhoto.get?.groupId)!)?.categoryTitle {
            self.infoCategoryLabel.text = categoryTitle
        }
        self.infoCopyrightLabel.text = self.viewModel.localPhoto.get?.photoAuthorFullName
        if (self.viewModel.localPhoto.get?.tags.count)! > 0 {
            let tagsString = self.viewModel.localPhoto.get?.tags.map { "#\($0.tagText)" }
            self.infoTagsLabel.text = tagsString?.joined(separator: " ")
        } else {
            self.infoTagsConstraint.priority = 750
        }
    }
    
    func checkComplaintButtonStatus() {
        if self.viewModel.photoDetail.get!.authorId == self.viewModel.repService.currentUserID.get {
            self.complaintButton.isHidden = true
        } else {
            self.complaintButton.isHidden = false
        }
    }
    
}

extension PhotoDetailViewController: MDCTabBarDelegate {
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        if tabBar.selectedItem == tabBar.items[0] {
            self.photoDetailContentScrollView.scrollRectToVisible(self.fullPhotoContentView.frame, animated: true)
        } else if tabBar.selectedItem == tabBar.items[1] {
            self.photoDetailContentScrollView.scrollRectToVisible(self.infoPhotoContentView.frame, animated: true)
        } else if tabBar.selectedItem == tabBar.items[2] {
            self.photoDetailContentScrollView.scrollRectToVisible(self.commentsPhotoContentView.frame, animated: true)
        }
    }
}

extension PhotoDetailViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.photoDetailContentScrollView {
            let fractionalPage = scrollView.contentOffset.x / scrollView.frame.width
            let pageTab = lround(Double(fractionalPage))
            if self.tabBarContainer.items.index(of: self.tabBarContainer.selectedItem!) != pageTab {
                self.tabBarContainer.setSelectedItem(self.tabBarContainer.items[pageTab], animated: true)
            }
        } else {
            if (scrollView.bounds.maxY == scrollView.contentSize.height) {
                //TODO painding
            }
        }
    }
}

extension PhotoDetailViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.photoComments.get.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //headerCell.delegate = self
        let commentCell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTabelCell
        commentCell.setComment(commentInfo: self.viewModel.photoComments.get[indexPath.row])
        commentCell.delegate = self
        return commentCell
    }
}

extension PhotoDetailViewController: CommentTabelCellDelegate {
    
    func complaintButtonCliced(cell: CommentTabelCell) {
        let indexPath = self.commentsPhotoTableView.indexPath(for: cell)
        alertController = UIAlertController(title: "Your complaint", message: "", preferredStyle: .alert)
        alertController?.addTextField { textField in
            textField.placeholder = ""
            textField.isSecureTextEntry = false
            textField.textAlignment = .left
        }
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            self.viewModel.complaintCommentText.set(value:(self.alertController?.textFields?.last?.text)! )
            self.viewModel.complaintCommentId.set(value:  self.viewModel.photoComments.get[(indexPath?.row)!].commentID)
            self.viewModel.complaintCommentsCommand.execute()
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            
        }
        alertController?.addAction(OKAction)
        alertController?.addAction(CancelAction)
        present(alertController!, animated: true, completion: nil)
    }
    
    func likeButtonCliced(cell: CommentTabelCell) {
    }
    
    func dislikeButtonCliced(cell: CommentTabelCell) {
    }
    
    func replyButtonCliced(cell: CommentTabelCell) {
    }
}
