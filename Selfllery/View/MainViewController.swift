//
//  MainViewController.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 05.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class MainViewController: ViewController {
    @IBOutlet weak var menuBarButtonItem: UIBarButtonItem!
    
    let viewModel = MainViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        viewModel.menuCommand <- self.menuBarButtonItem
        
        viewModel.menuCommand.subscribe { [unowned self] _ in
            if let drawerViewController = self.drawerViewController {
                drawerViewController.setDrawerState(.opened, animated: true)
            }
        }
    }
}
