//
//  PhotosViewController.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 12.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCollections

class PhotosViewController: ViewController {
    @IBOutlet weak var menuBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var tabBarContentView: UIView!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var photoChoiceCollectionView: UICollectionView!
    @IBOutlet weak var galeryCollectionView: UICollectionView!
    @IBOutlet weak var photosContentScrollView: UIScrollView!
    
    let viewModel = PhotosViewModel()
    
    lazy var tabBarContainer: MDCTabBar = {
        
        let tabBar = MDCTabBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.tabBarContentView.bounds.height))
        
        tabBar.items = [
            UITabBarItem(title: "POPULAR PHOTOS", image: nil, tag: 0),
            UITabBarItem(title: "EDITOR'S CHOICE", image: nil, tag: 0),
            UITabBarItem(title: "CATEGORIES", image: nil, tag: 0)
        ]
        
        tabBar.selectedItem = tabBar.items[0]
        tabBar.delegate = self
        tabBar.alignment = MDCTabBarAlignment.leading
        
        tabBar.tintColor = UIColor(hex: 0xeb6161)
        tabBar.selectedItemTintColor = UIColor(hex: 0xea5050)
        tabBar.unselectedItemTintColor = UIColor(hex: 0x969696)
        
        return tabBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarContentView.addSubview(self.tabBarContainer)
        self.viewModel.activeTabNumber.set(value: 0)
        
        viewModel.menuCommand <- self.menuBarButtonItem
        
        viewModel.menuCommand.subscribe { [unowned self] _ in
            if let drawerViewController = self.drawerViewController {
                drawerViewController.setDrawerState(.opened, animated: true)
            }
        }
        
        self.viewModel.popularPhotos.change.subscribe(onNext: { _ in
            if self.viewModel.activeTabNumber.get == 0 {
                self.photoCollectionView.reloadData()
            }
        })
        
        self.viewModel.editorChoicePhotos.change.subscribe(onNext: { _ in
            if self.viewModel.activeTabNumber.get == 1 {
                self.photoChoiceCollectionView.reloadData()
            }
        })
        
        self.viewModel.photosCategory.change.subscribe(onNext: { _ in
            if self.viewModel.activeTabNumber.get == 2 {
                self.galeryCollectionView.reloadData()
            }
        })
        
        self.viewModel.activeTabNumber.change.subscribe(onNext: { tabNumber in
            if tabNumber == 0 {
                self.photoCollectionView.reloadData()
            } else if tabNumber == 1 {
                self.photoChoiceCollectionView.reloadData()
            } else {
                self.galeryCollectionView.reloadData()
            }
        })
        
        self.viewModel.loadPopularPhotosCommand.execute()
        self.viewModel.loadEditorChoicePhotosCommand.execute()
        //self.viewModel.loaPhotosCategoryCommand.execute()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.loadPopularPhotosCommand.execute()
        self.viewModel.loadEditorChoicePhotosCommand.execute()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showGaleryDetail" {
            let galeryDetailController = segue.destination as! PhotosCollectionViewController
            galeryDetailController.viewModel.photosGaleryId.set(value: (self.viewModel.selectCategory.get?.categoryID)!)
            galeryDetailController.viewModel.photosGaleryName.set(value: (self.viewModel.selectCategory.get?.categoryTitle)!)
        }
        if segue.identifier == "showPhotoDetail" {
            let photoDetailController = segue.destination as! PhotoDetailViewController
            photoDetailController.viewModel.photoDetail.set(value: self.viewModel.selectPhoto.get)
        }
    }
}

extension PhotosViewController: MDCTabBarDelegate {
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        var index = 0
        if tabBar.selectedItem == tabBar.items[0] {
            index = 0
            self.photosContentScrollView.scrollRectToVisible(self.photoCollectionView.frame, animated: true)
        } else if tabBar.selectedItem == tabBar.items[1] {
            index = 1
            self.photosContentScrollView.scrollRectToVisible(self.photoChoiceCollectionView.frame, animated: true)
        } else if tabBar.selectedItem == tabBar.items[2] {
            index = 2
            self.photosContentScrollView.scrollRectToVisible(self.galeryCollectionView.frame, animated: true)
        }
        self.viewModel.activeTabNumber.set(value: index)
    }
}

extension PhotosViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.photosContentScrollView {
            let fractionalPage = scrollView.contentOffset.x / scrollView.frame.width
            let pageTab = lround(Double(fractionalPage))
            if self.tabBarContainer.items.index(of: self.tabBarContainer.selectedItem!) != pageTab {
                self.viewModel.activeTabNumber.set(value: pageTab)
                self.tabBarContainer.setSelectedItem(self.tabBarContainer.items[pageTab], animated: true)
            }
        } else {
            if (scrollView.bounds.maxY == scrollView.contentSize.height) {
                if scrollView == self.photoCollectionView {
                    self.viewModel.loadPopularPage.set(value: self.viewModel.loadPopularPage.get + 1)
                    self.viewModel.loadPopularPhotosCommand.execute()
                } else if scrollView == self.photoChoiceCollectionView {
                    self.viewModel.loadEditorChoicePage.set(value: self.viewModel.loadEditorChoicePage.get + 1)
                    self.viewModel.loadEditorChoicePhotosCommand.execute()
                }
            }
        }
    }
}

extension PhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.photoCollectionView {
            return self.viewModel.popularPhotos.get.count
        } else if collectionView == self.photoChoiceCollectionView {
            return self.viewModel.editorChoicePhotos.get.count
        }
        return self.viewModel.photosCategory.get.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.galeryCollectionView {
            let category = self.viewModel.photosCategory.get[indexPath.row]
            self.viewModel.selectCategory.set(value: category)
            self.performSegue(withIdentifier: "showGaleryDetail", sender: self)
        } else {
            var photo: SLPhoto? = nil
            if collectionView == self.photoCollectionView {
                photo = self.viewModel.popularPhotos.get[indexPath.row]
            } else if collectionView == self.photoChoiceCollectionView {
                photo = self.viewModel.editorChoicePhotos.get[indexPath.row]
            }
            self.viewModel.selectPhoto.set(value: photo)
            self.performSegue(withIdentifier: "showPhotoDetail", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var usableSpace = collectionView.frame.width
        var cellWidth: CGFloat = 0.0
        var cellHeight: CGFloat = 0.0
        if collectionView == self.galeryCollectionView {
            usableSpace = usableSpace - 2 * collectionViewContentInset
            cellWidth = usableSpace
            cellHeight = usableSpace * 9.0 / 16.0
        } else {
            usableSpace = usableSpace - 2 * collectionViewContentInset - 2 * collectionViewCellContentInset
            cellWidth = usableSpace / 3.0
            cellHeight = usableSpace / 3.0
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCollectionCell
        if collectionView == self.galeryCollectionView {
            let category = self.viewModel.photosCategory.get[indexPath.row]
            cell.setCategory(name: category.categoryTitle)
            cell.setWidePhotoImage(photoUrl: category.backgroundUrl)
            return cell
        }
        var photo: SLPhoto? = nil
        if collectionView == self.photoCollectionView {
            photo = self.viewModel.popularPhotos.get[indexPath.row]
        } else if collectionView == self.photoChoiceCollectionView {
            photo = self.viewModel.editorChoicePhotos.get[indexPath.row]
        }
        cell.setSmallPhotoImage(photoUrl: photo?.photoUrl)
        return cell
    }
}
