//
//  EditProfileViewController.swift
//  Selfllery
//
//  Created by victor on 11.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material
import MaterialComponents
//import MaterialComponents.MaterialCollections

class EditProfileViewController: ViewController  {
    @IBOutlet weak var menuBarButtonItem: UIBarButtonItem!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var confirmChagesButton: UIBarButtonItem!
    
    @IBOutlet weak var changeAvatarImageView: UIImageView!
    @IBOutlet weak var changeBackgroundImageView: UIImageView!
    
    
    @IBOutlet weak var firstNameTextField: TextField!
    @IBOutlet weak var lastNameTextField: TextField!
    @IBOutlet weak var emailTextField: TextField!
    

    @IBOutlet weak var birthDaytextField: TextField!
    @IBOutlet weak var genderTextField: TextField!
    @IBOutlet weak var countryTextField: TextField!
    
    var isAvatarWillChange: Bool = false
    var datePicker = UIDatePicker()
    var pickerView = UIPickerView()
    var pickerData = Array<Any>()
    var genderPickerData = ["Male","Female"]
    
    let viewModel = EditProfileViewModel()
    
        let avatarImagePickerController: UIImagePickerController = UIImagePickerController()
        lazy var avatarImagePickerAlert: UIAlertController = {
            let controller = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
            let camera = UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.avatarImagePickerController.sourceType = .camera
                self.present(self.avatarImagePickerController, animated: true, completion:nil)
            })
            let photos = UIAlertAction(title: "Photo Library", style: .default, handler: { _ in
                self.avatarImagePickerController.sourceType = .photoLibrary
                self.present(self.avatarImagePickerController, animated: true, completion:nil)
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            controller.addAction(camera)
            controller.addAction(photos)
            controller.addAction(cancel)
            return controller
        }()
    
    let backgroundImagePickerController: UIImagePickerController = UIImagePickerController()
    lazy var backgroundImagePickerAlert: UIAlertController = {
        let controller = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.avatarImagePickerController.sourceType = .camera
            self.present(self.avatarImagePickerController, animated: true, completion:nil)
        })
        let photos = UIAlertAction(title: "Photo Library", style: .default, handler: { _ in
            self.avatarImagePickerController.sourceType = .photoLibrary
            self.present(self.avatarImagePickerController, animated: true, completion:nil)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(camera)
        controller.addAction(photos)
        controller.addAction(cancel)
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avatarImageView.cornerRadius = avatarImageView.frame.height/2.0
        self.title = "Edit Profile"
        
        let tapGestureRecognizerAvatar = UITapGestureRecognizer(target: self, action: #selector(avatarImageTapped(tapGestureRecognizer:)))
        changeAvatarImageView.isUserInteractionEnabled = true
        changeAvatarImageView.addGestureRecognizer(tapGestureRecognizerAvatar)
        
        let tapGestureRecognizerBackground = UITapGestureRecognizer(target: self, action: #selector(backgroundImageView(tapGestureRecognizer:)))
        changeBackgroundImageView.isUserInteractionEnabled = true
        changeBackgroundImageView.addGestureRecognizer(tapGestureRecognizerBackground)

        avatarImagePickerController.delegate = self
        birthDaytextField.delegate = self
        genderTextField.delegate = self
        countryTextField.delegate = self
        
        if viewModel.isOwnProfile.get {
            let backButton = UIBarButtonItem(image: UIImage(named: "ico_back"), style: .plain, target: nil, action: nil)
            self.navigationItem.leftBarButtonItem = backButton
            viewModel.backCommand <- backButton
        } else {
            viewModel.menuCommand <- self.menuBarButtonItem
        }
        viewModel.confirmChangesCommand <- self.confirmChagesButton
        
        viewModel.genderProperty <-> self.genderTextField
        viewModel.birthdayDateProperty <-> self.birthDaytextField
        viewModel.nameFirstProperty <-> self.firstNameTextField
        viewModel.nameLastProperty <-> self.lastNameTextField
        viewModel.emailProperty <-> self.emailTextField
        
        viewModel.confirmChangesCommand.subscribe {  [unowned self] result in
            switch result {
            case .Success(value: ):
                self.performSegue(withIdentifier: "eventSegue", sender: self)
            case .Failure(let error):
                print(error)
            }
        }
        
        self.viewModel.backCommand.subscribe { [unowned self] _ in
            self.navigationController?.popViewController(animated: true)
        }
        
        viewModel.menuCommand.subscribe { [unowned self] _ in
            if self.firstNameTextField.isFirstResponder {
                self.firstNameTextField.resignFirstResponder()
            }
            if self.lastNameTextField.isFirstResponder {
                self.lastNameTextField.resignFirstResponder()
            }
            if self.emailTextField.isFirstResponder {
                self.emailTextField.resignFirstResponder()
            }
            if self.birthDaytextField.isFirstResponder {
                self.birthDaytextField.resignFirstResponder()
            }
            
            if self.genderTextField.isFirstResponder {
                self.genderTextField.resignFirstResponder()
            }
            if self.countryTextField.isFirstResponder {
                self.countryTextField.resignFirstResponder()
            }
            if let drawerViewController = self.drawerViewController {
                drawerViewController.setDrawerState(.opened, animated: true)
            }
        }
        
        self.UISetup()
        self.scrollView.startListeningForKeyboardNotification()
        pickerData = self.viewModel.countiesArray.get.map { $0.countryName }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.scrollView.stopListenningForKeyboardNotification()
    }
    
    func UISetup() {
        let imageCache = PersistentAutoPurgingImageCache.default
        if let linkUrlString = self.viewModel.ownerUser.get?.avatarSmalUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.avatarImageView.image = Utilities.sharedInstance.fixOrientation(image)
            } else {
                self.avatarImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }

        if self.viewModel.ownerUser.get?.backgroundUrl != "" && self.viewModel.ownerUser.get?.backgroundUrl != nil {
            if let linkUrlString = self.viewModel.ownerUser.get?.backgroundUrl, let linkUrl = URL(string: linkUrlString) {
                if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                    self.backgroundImageView.image = Utilities.sharedInstance.fixOrientation(image)
                } else {
                    self.backgroundImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                        if let image = dataResponse.result.value {
                            imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                        }
                    })
                }
            }
        }
                
        self.prepareTextFields(textField: firstNameTextField)
        self.prepareTextFields(textField: lastNameTextField)
        self.prepareTextFields(textField: emailTextField)
        
        viewModel.nameFirstProperty.set(value: viewModel.ownerUser.get!.firstName)
        viewModel.nameLastProperty.set(value: viewModel.ownerUser.get!.lastName)
        viewModel.emailProperty.set(value: viewModel.ownerUser.get!.email)
        viewModel.birthdayDateProperty.set(value: viewModel.ownerUser.get!.userBirthDate())
        self.countryTextField.text = viewModel.ownerUser.get?.countryName
        let gender = (viewModel.ownerUser.get!.gender == "0") ?"Male" :"Female"
        viewModel.genderProperty.set(value: gender)
    }
    
    func prepareTextFields(textField: TextField) {
        textField.delegate = self
        textField.clearButtonMode = .whileEditing
        textField.dividerActiveColor = Color.red.base
        textField.placeholderNormalColor = Color.grey.base
        textField.placeholderActiveColor = Color.grey.base
        textField.isVisibilityIconButtonEnabled = false
        
        textField.placeholderLabel.font = UIFont (name: "Roboto-Regular", size: 16)
        
        textField.textInset = 0
        
        switch textField {
        case firstNameTextField:
            textField.placeholder = "First Name"
        case lastNameTextField:
            textField.placeholder = "Last Name"
        case emailTextField:
            textField.placeholder = "Email"
        default:
            textField.placeholder = ""
        }
    }
    
    //MARK:UITapGestureRecognizer
    func avatarImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        isAvatarWillChange = true
        self.present(self.avatarImagePickerAlert, animated: true, completion: nil)
    }
    
    func backgroundImageView(tapGestureRecognizer: UITapGestureRecognizer) {
        isAvatarWillChange = false
        self.present(self.backgroundImagePickerAlert, animated: true, completion: nil)
    }

    func pickUpDate(textField : UITextField, titleText: String) {
        
        if textField == birthDaytextField {
            self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.datePicker.backgroundColor = UIColor.white
            self.datePicker.datePickerMode = UIDatePickerMode.date
            textField.inputView = self.datePicker
        }
        
        if textField == genderTextField {
            self.pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerView.backgroundColor = UIColor.white
            pickerView.dataSource = self
            pickerView.delegate = self
            
            textField.inputView = self.pickerView
        }
        
        if textField == countryTextField {
            self.pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerView.backgroundColor = UIColor.white
            pickerView.dataSource = self
            pickerView.delegate = self
            
            textField.inputView = self.pickerView
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.backgroundColor = UIColor(hex: 0xf6f6f6, alpha: 0.3)
        toolBar.tintColor = UIColor(hex: 0x157efb)
        toolBar.sizeToFit()
        
        let title = UILabel()
        title.text = titleText
        title.textColor = UIColor.black
        title.font = UIFont (name: "Roboto-Regular", size: 17)
        let titleItem = UIBarButtonItem(customView: title)
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(EditProfileViewController.doneClick(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(EditProfileViewController.cancelClick(_:)))
        toolBar.setItems([cancelButton, spaceButton,titleItem, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        textField.tintColor = UIColor.clear
    }
    
    func doneClick(_ textField: UITextField) {
        if  birthDaytextField.isFirstResponder {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateString = dateFormatter.string(from: datePicker.date)
            viewModel.birthdayDateProperty.set(value: dateString)
            birthDaytextField.resignFirstResponder()
        }
        
        if  genderTextField.isFirstResponder {
            genderTextField.resignFirstResponder()
        }
        
        if countryTextField.isFirstResponder  {
            countryTextField.resignFirstResponder()
        }
    }
    
    func cancelClick(_ textField: UITextField) {
        if birthDaytextField.isFirstResponder {
            viewModel.birthdayDateProperty.set(value: viewModel.ownerUser.get!.userBirthDate())
            birthDaytextField.resignFirstResponder()
        }
        
        if genderTextField.isFirstResponder {
            let gender = (viewModel.ownerUser.get!.gender == "0") ?"Male" :"Female"
            viewModel.genderProperty.set(value: gender)
            genderTextField.resignFirstResponder()
        }
        
        if  countryTextField.isFirstResponder {
            self.countryTextField.text = viewModel.ownerUser.get?.countryName
            countryTextField.resignFirstResponder()
        }
    }
}
//MARK: UIPickerViewDelegate
extension EditProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if genderTextField.isFirstResponder { return self.genderPickerData.count }
        return self.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if genderTextField.isFirstResponder {
            return self.genderPickerData[row]
        }
        return self.pickerData[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if genderTextField.isFirstResponder {
            let gender = self.genderPickerData[row]
            self.viewModel.genderProperty.set(value: gender)
        }
        if  countryTextField.isFirstResponder {
            countryTextField.text = self.pickerData[row] as? String
            self.viewModel.countryIdProperty.set(value: row + 1)
        }
    }
    
}

//MARK:UIImagePickerControllerDelegate
extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        if isAvatarWillChange {
            self.avatarImageView.image = image
            self.viewModel.avatarImage.set(value: Utilities.sharedInstance.fixOrientation(image!))
            self.viewModel.uploadAvatarImageCommand.execute()
        } else {
            self.backgroundImageView.image = image
            self.viewModel.backgroundImage.set(value: image)
            self.viewModel.uploadBackgroundImageCommand.execute()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK:TextFieldDelegate
extension EditProfileViewController: TextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case birthDaytextField:
            self.pickUpDate(textField: birthDaytextField, titleText: "Date of Birth")
        case genderTextField:
            self.pickUpDate(textField: genderTextField, titleText: "Sex")
            let selInd = (self.viewModel.genderProperty.get == "Male" ?0 :1)
            self.pickerView.selectRow(selInd, inComponent: 0, animated: false)
        case countryTextField:
            self.pickUpDate(textField: countryTextField, titleText: "Country")
        default:
            break
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = true
        return true
    }
}



