//
//  HeaderMenuTableCell.swift
//  Selfllery
//
//  Created by victor on 10.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material

class HeaderMenuTableCell: TableViewCell {
    
    weak var delegate: HeaderMenuCellDelegate?
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var token: UILabel!
    
    override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsets.zero }
        set(newVal) {}
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        inititalize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inititalize()
    }
    
    func inititalize() {
        self.avatarImageView.cornerRadius = 20
    }
    
    @IBAction func showProfileClick(button: UIButton) {
        self.delegate?.showUserProfileButtonCliced(headerCell: self)
    }
    
    func setCurrent(user: SLUser) {
        self.nameLbl.text = user.fullName
        self.emailLbl.text = user.email
        //self.token.text =  "Tokens:  \(String(user.tokensCount))"
        self.token.isHidden = true
        
        let imageCache = PersistentAutoPurgingImageCache.default
        if let linkUrlString = user.avatarSmalUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.avatarImageView.image = image
            } else {
                self.avatarImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }
        if user.backgroundUrl != "" && user.backgroundUrl != nil {
            if let linkUrlString = user.backgroundUrl, let linkUrl = URL(string: linkUrlString) {
                if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                    self.backgroundImageView.image = image
                } else {
                    self.backgroundImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                        if let image = dataResponse.result.value {
                            imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                        }
                    })
                }
            }
        }
    }
    
}

protocol HeaderMenuCellDelegate: class {
    func showUserProfileButtonCliced(headerCell: HeaderMenuTableCell)
}
