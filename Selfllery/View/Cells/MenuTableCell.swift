//
//  MenuTableCell.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 09.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material

class MenuTableCell: TableViewCell {
    
    @IBOutlet weak var menuNameLabel: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var bageLbl: UILabel!
    
    override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsets.zero }
        set(newVal) {}
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        inititalize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inititalize()
    }
    
    func inititalize() {
        self.bageLbl.text = "1"
        self.bageLbl.layer.cornerRadius = 8
        self.bageLbl.clipsToBounds = true
        self.bageLbl.isHidden = true
    }
    
    func setContent(indexPath: IndexPath) {
        menuNameLabel.text = self.menuName(withSection: indexPath.section, withIndex: indexPath.row)
        imageCell.image = self.leftIcon(withSection: indexPath.section, withIndex: indexPath.row)
//        if indexPath.section == 1 {
//            bageLbl.isHidden = indexPath.row != 0
//        }
    }
    
    func selectMenu(select: Bool) {
        self.menuNameLabel.textColor = (select == true) ?UIColor(hex: 0xea5050) :UIColor.darkText
        self.imageCell.tintColor = (select == true) ?UIColor(hex: 0xea5050) :UIColor.darkGray
    }
    
    func leftIcon(withSection: Int, withIndex: Int) -> UIImage {
        if withSection == 2 {
            switch withIndex {
            case 0:
                return UIImage(named: "ico-blog")!//return UIImage(named: "ico-photos")!
//            case 1:
//                return UIImage(named: "ico-blog")!
            default:
                return UIImage(named: "")!
            }
        } else if withSection == 3 {
            return UIImage(named: "ico-exit")!
        } else {
            switch withIndex {
            case 0:
                  return UIImage(named: "ico-photos")! //return UIImage(named: "ico-notifications-copy")!
            case 1:
                return  UIImage(named: "ico-add")!
            case 2:
                return  UIImage(named: "ico-edit")!
            default:
                return UIImage(named: "")!
            }
        }
    }
    
    func leftIconWithSelected(withSection: Int, withIndex: Int) -> UIImage {
        if withSection == 2 {
            switch withIndex {
            case 0:
                return UIImage(named: "ico-blog")!
            default:
                return UIImage(named: "")!
            }
        } else if withSection == 3 {
            return UIImage(named: "ico-exit")!
        } else {
            switch withIndex {
            case 0:
                return UIImage(named: "ico-photos")! //return UIImage(named: "ico-notifications-copy")!
            case 1:
                return  UIImage(named: "ico-add")!
            case 2:
                return  UIImage(named: "ico-edit")!
            default:
                return UIImage(named: "")!
            }
        }
    }
    
    func menuName(withSection: Int, withIndex: Int) -> String {
        if withSection == 2 {
            switch withIndex {
            case 0:
                return "About"//return "Photo"
//            case 1:
//                return "About"
            default:
                return ""
            }
        } else if withSection == 3 {
            return "Exit"
        } else {
            switch withIndex {
            case 0:
                return "Photo"
            case 1:
                return "Upload photo"
            case 2:
                return "Edit profile"

            default:
                return ""
            }
        }
    }
}
