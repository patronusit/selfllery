//
//  PhotoCollectionCell.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 14.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

var collectionViewCellContentInset: CGFloat = 8.0
var collectionViewContentInset: CGFloat = 4.0

class PhotoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    
    func setCategory(name: String) {
        self.nameLabel.text = name
    }
    
    func setWidePhotoImage(photoUrl: String?) {
        let fullUrl = photoUrl! + PhotoResolutionName.wide320.rawValue + ".jpg"
        self.setPhotoImage(photoUrl: fullUrl)
    }
    
    func setSmallPhotoImage(photoUrl: String?) {
        let fullUrl = photoUrl! + PhotoResolutionName.square150.rawValue + ".jpg"
        self.setPhotoImage(photoUrl: fullUrl)
    }
    
    func setPhoto(image: UIImage)  {
        self.photoImageView.image = image
    }
    
    func selectCell() {
        self.selectedView.isHidden = !self.selectedView.isHidden
    }
    
    
    func setPhotoImage(photoUrl: String?) {
        let imageCache = PersistentAutoPurgingImageCache.default
        if let linkUrlString = photoUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.photoImageView.image = image
            } else {
                self.photoImageView.image = nil
                self.photoImageView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }
    }
}
