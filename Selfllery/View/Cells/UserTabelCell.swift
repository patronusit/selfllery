//
//  UserTabelCell.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 21.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import Material

class UserTableCell: TableViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userAvatarImage: UIImageView!
    
    override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsets.zero }
        set(newVal) {}
    }
    
    func setUser(userInfo: SLUser) {
        self.userNameLabel.text = userInfo.fullName
        
        let imageCache = PersistentAutoPurgingImageCache.default
        let avatarUrl = userInfo.avatarUrl
        if let linkUrlString = avatarUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.userAvatarImage.image = image
            } else {
                self.userAvatarImage.image = nil
                self.userAvatarImage.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }
    }
}
