//
//  UserSettingTabelCell.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 01.03.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import Material

class UserSettingTabelCell: TableViewCell {
    
    @IBOutlet weak var settingTitleLabel: UILabel!
    @IBOutlet weak var settingCheckboxImageView: UIImageView!
    
    var isCheck = false
    
    override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsets.zero }
        set(newVal) {}
    }
    
    func setUserSetting(settingInfo: SLSetting) {
        if let charityInfo = SLRepository.sharedRepository.getCharity(byId: settingInfo.charityCategoryID) {
            self.settingTitleLabel.text = charityInfo.charityText
        }
        
        if settingInfo.charityState {
            isCheck = true
            self.settingCheckboxImageView.image = UIImage(named: "ico_checked")!
        } else {
            isCheck = false
            self.settingCheckboxImageView.image = UIImage(named: "ico_unchecked")!
        }
    }
    
    func setCheck() {
        self.isCheck = !self.isCheck
        if isCheck {
            self.settingCheckboxImageView.image = UIImage(named: "ico_checked")!
        } else {
             self.settingCheckboxImageView.image = UIImage(named: "ico_unchecked")!
        }
    }
    
}
