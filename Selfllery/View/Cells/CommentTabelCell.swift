//
//  CommentTabelCell.swift
//  Selfllery
//
//  Created by Victor on 2/26/18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material

class CommentTabelCell: TableViewCell {
    @IBOutlet weak var avatarImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var commenttext: UILabel!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var complaintButton: UIButton!
    
    weak var delegate: CommentTabelCellDelegate?
    
    override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsets.zero }
        set(newVal) {}
    }
    
    func setComment(commentInfo: SLComment) {
        let imageCache = PersistentAutoPurgingImageCache.default
        let avatarUrl = commentInfo.avatarUrl
        if let linkUrlString = avatarUrl, let linkUrl = URL(string: linkUrlString) {
            if let image = imageCache.image(withIdentifier: linkUrl.lastPathComponent) {
                self.avatarImgView.image = image
            } else {
                self.avatarImgView.image = nil
                self.avatarImgView.af_setImage(withURL: linkUrl, completion: { dataResponse in
                    if let image = dataResponse.result.value {
                        imageCache.add(image, withIdentifier: linkUrl.lastPathComponent)
                    }
                })
            }
        }
        
        self.nameLbl.text = commentInfo.fullName
        self.timeLbl.text = commentInfo.commentDateString()
        self.commenttext.text = commentInfo.commentText
        self.self.likeCountLbl.text = String(commentInfo.usefulness)
    }
    
    @IBAction func likeClick(button: UIButton) {
        self.delegate?.likeButtonCliced(cell: self)
    }
    @IBAction func dislikeClick(button: UIButton) {
        self.delegate?.dislikeButtonCliced(cell: self)
    }
    
    @IBAction func replyClick(button: UIButton) {
        self.delegate?.dislikeButtonCliced(cell: self)
    }
    
    @IBAction func complaintClick(button: UIButton) {
        self.delegate?.complaintButtonCliced(cell: self)
    }
}

protocol CommentTabelCellDelegate: class {
    func likeButtonCliced(cell: CommentTabelCell)
    func dislikeButtonCliced(cell: CommentTabelCell)
    func replyButtonCliced(cell: CommentTabelCell)
    func complaintButtonCliced(cell: CommentTabelCell)
    
}

