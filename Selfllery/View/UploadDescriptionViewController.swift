//
//  UploadDescriptionViewController.swift
//  Selfllery
//
//  Created by Victor on 2/21/18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import Material
import MaterialComponents
import AVFoundation

class UploadDescriptionViewController: ViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var categoryTextField: TextField!
    @IBOutlet weak var titleTextField: TextField!
    @IBOutlet weak var descriptionTextField: TextField!
    @IBOutlet weak var tagsTextField: TextField!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var uploadButton: SLButton!
    @IBOutlet weak var addPhotoButton: SLButton!
    @IBOutlet weak var preViewImageView: UIImageView!
    
    let viewModel = UploadDescriptionViewModel()
    var pickerView = UIPickerView()
    var pickerData = Array<Any>()
    var selecdedCategoryId: Int = 0
    
    let addImagePickerController: UIImagePickerController = UIImagePickerController()
    lazy var addImagePickerAlert: UIAlertController = {
        let controller = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: { _ in
            let cameraMediaType = AVMediaTypeVideo
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
            switch cameraAuthorizationStatus {
            case .denied:
                let alert = UIAlertController.settingsAllert()
                self.present(alert, animated: true) {}
            case .authorized:
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    self.addImagePickerController.sourceType = .camera
                    self.present(self.addImagePickerController, animated: true, completion:nil)
                }
            case .restricted:
                break
            case .notDetermined:
                AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
                    if granted {
                        if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        self.addImagePickerController.sourceType = .camera
                        self.present(self.addImagePickerController, animated: true, completion:nil)
                        }
                        //print("Granted access to \(cameraMediaType)")
                    } else {
                        //print("Denied access to \(cameraMediaType)")
                    }
                }
            }
        })
        
        let photos = UIAlertAction(title: "Photo Library", style: .default, handler: { _ in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.addImagePickerController.sourceType = .photoLibrary
                self.present(self.addImagePickerController, animated: true, completion:nil)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(camera)
        controller.addAction(photos)
        controller.addAction(cancel)
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Upload Photo"
        
        self.addImagePickerController.delegate = self
        self.viewModel.backCommand <- backButton
        self.viewModel.uploadPhotoCommand <- uploadButton
        self.viewModel.addPhotoCommand <- addPhotoButton
        
        self.viewModel.titleProperty <-> self.titleTextField
        self.viewModel.descriptionProperty <-> self.descriptionTextField
        self.viewModel.tagsProperty <-> self.tagsTextField
       // self.viewModel.loadCategoryesCommand.execute()
        
        self.prepareTextFields(textField: titleTextField)
        self.prepareTextFields(textField: descriptionTextField)
        self.prepareTextFields(textField: tagsTextField)
        
        titleTextField.delegate = self
        descriptionTextField.delegate = self
        tagsTextField.delegate = self
        categoryTextField.delegate = self
        
        self.viewModel.addPhotoCommand.subscribe {
            self.present(self.addImagePickerAlert, animated: true, completion: nil)
        }
        
        self.viewModel.uploadPhotoCommand.before {
            self.userInteractionEnabled(status: false)
        }
        
        self.viewModel.uploadPhotoCommand.subscribe { [unowned self] result in
            self.userInteractionEnabled(status: true)
            switch result {
            case .Success(let value):
                if value {
                    self.viewModel.approvePhotoCommand.execute()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kOpenMainScreen"), object: self)
                }
            case .Failure(let error):
                switch error {
                case BackendError.Error(message: let message):
                    let alert = UIAlertController.customAlertController(title: "Error!", message: message)
                    self.present(alert, animated: true) {}
                default:
                    break
                }
            }
        }
        
        self.viewModel.backCommand.subscribe { [unowned self] _ in
            if self.titleTextField.isFirstResponder {
                self.titleTextField.resignFirstResponder()
            }
            if self.categoryTextField.isFirstResponder {
                self.categoryTextField.resignFirstResponder()
            }
            if self.descriptionTextField.isFirstResponder {
                self.descriptionTextField.resignFirstResponder()
            }
            if self.tagsTextField.isFirstResponder {
                self.tagsTextField.resignFirstResponder()
            }
            if let drawerViewController = self.drawerViewController {
                drawerViewController.setDrawerState(.opened, animated: true)
            }
        }
        
        self.viewModel.uploadPhotoCommand.before.subscribe { [unowned self] _ in
            let tagsStringsArray = self.viewModel.tagsProperty.get.components(separatedBy: " ")
            var tagArray: [String] = []
            for tagString in tagsStringsArray {
                let clearTag = tagString.replacePunctuationCharacters()
                if clearTag != "" {
                    tagArray.append(clearTag)
                }
            }
            self.viewModel.tagsArrayProperty.set(value: tagArray)
        }
        
    }
    
    func userInteractionEnabled(status:Bool) {
        self.view.isUserInteractionEnabled = status
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.scrollView.startListeningForKeyboardNotification()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.scrollView.stopListenningForKeyboardNotification()
    }
    
    func prepareTextFields(textField: TextField) {
        textField.delegate = self
        textField.clearButtonMode = .whileEditing
        textField.dividerActiveColor = Color.red.base
        textField.placeholderNormalColor = Color.grey.base
        textField.placeholderActiveColor = Color.grey.base
        textField.isVisibilityIconButtonEnabled = false
        textField.placeholderLabel.font = UIFont (name: "Roboto-Regular", size: 16)
        textField.textInset = 0
        
        switch textField {
        case titleTextField:
            textField.placeholder = "Title"
        case descriptionTextField:
            textField.placeholder = "Description"
        case tagsTextField:
            textField.placeholder = "Tags"
        default:
            textField.placeholder = ""
        }
    }
    
    func pickUpDate(textField : UITextField, titleText: String) {
        if textField == categoryTextField {
            self.pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerView.backgroundColor = UIColor.white
            pickerView.dataSource = self
            pickerView.delegate = self
            textField.inputView = self.pickerView
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.backgroundColor = UIColor(hex: 0xf6f6f6, alpha: 0.3)
        toolBar.tintColor = UIColor(hex: 0x157efb)
        toolBar.sizeToFit()
        
        let title = UILabel()
        title.text = titleText
        title.textColor = UIColor.black
        title.font = UIFont (name: "Roboto-Regular", size: 17)
        let titleItem = UIBarButtonItem(customView: title)
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(EditProfileViewController.doneClick(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(EditProfileViewController.cancelClick(_:)))
        toolBar.setItems([cancelButton, spaceButton,titleItem, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        textField.tintColor = UIColor.clear
    }
    
    func doneClick(_ textField: UITextField) {
        if  categoryTextField.isFirstResponder {
            categoryTextField.resignFirstResponder()
        }
    }
    
    func cancelClick(_ textField: UITextField) {
        if categoryTextField.isFirstResponder {
            //self.birthDaytextField.text = viewModel.ownerUser.get?.userBirthDate()
            categoryTextField.resignFirstResponder()
        }
    }
}

//MARK: UIPickerViewDelegate
extension UploadDescriptionViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.viewModel.photosCategory.get.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.viewModel.photosCategory.get[row].categoryTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if categoryTextField.isFirstResponder {
            categoryTextField.text = self.viewModel.photosCategory.get[row].categoryTitle
            self.viewModel.selectedCategoryId.set(value: self.viewModel.photosCategory.get[row].categoryID)
        }
    }
}

//MARK:UIImagePickerControllerDelegate
extension UploadDescriptionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.preViewImageView.image = image
        self.viewModel.photoImage.set(value: image)
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK:TextFieldDelegate
extension UploadDescriptionViewController: TextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case categoryTextField:
            self.pickUpDate(textField: categoryTextField, titleText: "Category")
//        case genderTextField:
//            self.pickUpDate(textField: genderTextField, titleText: "Sex")
//        case countryTextField:
//            self.pickUpDate(textField: countryTextField, titleText: "Country")
        default:
            break
        }
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }

    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.titleTextField {
            self.categoryTextField.becomeFirstResponder()
            return true
        }
        if textField == self.categoryTextField {
            self.descriptionTextField.becomeFirstResponder()
            return true
        }
        if textField == self.descriptionTextField {
            self.tagsTextField.becomeFirstResponder()
            return true
        }
        if textField == self.tagsTextField {
            self.tagsTextField.resignFirstResponder()
        }
        (textField as? ErrorTextField)?.isErrorRevealed = true
        return true
    }
}

