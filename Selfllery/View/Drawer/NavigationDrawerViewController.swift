//
//  NavigationDrawerViewController.swift
//  Oras
//
//  Created by Ivan Zagorulko on 28.12.16.
//  Copyright © 2016 Patronus IT. All rights reserved.
//

import UIKit
import KYDrawerController
import GoogleSignIn
class NavigationDrawerViewController: ViewController, KYDrawerControllerDelegate {
    
    @IBOutlet weak var menuTableView: UITableView!
    
    let viewModel = MainViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headerCellNib = UINib(nibName: "HeaderMenuTableCell", bundle: nil)
        menuTableView.register(headerCellNib, forCellReuseIdentifier: "headerCell")
        
        //self.tableView.register(nib, forCellReuseIdentifier: "headerCell")
        
//        self.versionLabel.text = Utilities.sharedInstance.versionString()
//        
//        let login = UserDefaults.standard.value(forKey: "account_login") as! String
//        self.userNameLabel.text = login
//        
//        let currentUser = OProjectRepository.sharedRepository.currentUser.get
//        self.userEmailLabel.text = currentUser?.email
//        
//        OProjectRepository.sharedRepository.currentUser.change.subscribe(onNext: { [unowned self] user in
//            let userGroups = user?.groups.toArray().map { $0.groupName! }
//            self.userGroupsLabel.text = userGroups?.joined(separator: "/ ")
//        })
//        
//        let userGroups = currentUser?.groups.toArray().map { $0.groupName! }
//        self.userGroupsLabel.text = userGroups?.joined(separator: "/ ")
        
        self.viewModel.selectMenu.set(value: IndexPath(row: 0, section: 1))

        // Do any additional setup after loading the view.
        if let drawerViewController = self.drawerViewController {
            drawerViewController.delegate = self
        }
        
        self.viewModel.user.change.subscribe(onNext: { userInfo in
            if userInfo != nil {
                self.menuTableView.reloadData()
            }
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(openMainScreen), name: NSNotification.Name(rawValue: "kOpenMainScreen"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func logOut() {
        guard let drawerViewController = self.parent as? DrawerViewController else { return }
        let confirmAlert = UIAlertController(title: "Exit", message: "Do you want to exit?", preferredStyle: .alert)
        confirmAlert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.default, handler: { action in
            GIDSignIn.sharedInstance().signOut()
            SLAuthenticateService.sharedInstanse.invalidateAuthenticationCredentials()
            drawerViewController.performSegue(withIdentifier: "Exit", sender: nil)
        }))
        confirmAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
        self.present(confirmAlert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func openMainScreen() {
        self.selectMenu(indexPath: IndexPath(row: 0, section: 1))
    }
    
    func selectMenu(indexPath: IndexPath) {
        var mainViewController: UIViewController?
        if indexPath.section == 2 {
            switch indexPath.row {
            case 0:
                mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllerName.AboutViewControllerName.rawValue)
               // mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllerName.PhotosViewControllerName.rawValue)
//            case 1:
//                mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllerName.AboutViewControllerName.rawValue)
            default:
                mainViewController = nil
            }
            
        } else if indexPath.section == 3 {
            self.logOut()
        } else {
            switch indexPath.row {
            case 0:
                mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllerName.PhotosViewControllerName.rawValue)
                //mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllerName.HomeViewControllerName.rawValue)
            case 1:
                mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllerName.UploadPhotosViewControllerName.rawValue)
            case 2:
                mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllerName.EditProfileViewControllerName.rawValue)
            default:
                mainViewController = nil
            }
        }
        let parentController: KYDrawerController = self.parent as! KYDrawerController
        if mainViewController != nil {
            parentController.mainViewController = mainViewController
        }
        parentController.setDrawerState(.closed, animated: true)
    }

}

extension NavigationDrawerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 && indexPath.section == 0 {
            return 154
        } else {
            return 55
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 3
        case 2:
            return 1
        case 3:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1.0))
        headerView.backgroundColor = UIColor(hex: 0xd8d8d8)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectMenu(indexPath: indexPath)
        if indexPath.section != 0 {
            if let selMenu = self.viewModel.selectMenu.get {
                let unselectCell = tableView.cellForRow(at: selMenu) as! MenuTableCell
                unselectCell.selectMenu(select: false)
            }
            self.viewModel.selectMenu.set(value: indexPath)
            let cell = tableView.cellForRow(at: indexPath) as! MenuTableCell
            cell.selectMenu(select: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! HeaderMenuTableCell
            if let userInfo = self.viewModel.user.get {
                headerCell.setCurrent(user: userInfo)
                headerCell.delegate = self
            }
            return headerCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuTableCell
            if indexPath == self.viewModel.selectMenu.get! {
                cell.selectMenu(select: true)
            }
            cell.setContent(indexPath: indexPath)
            return cell
        }
    }    
}

extension NavigationDrawerViewController: HeaderMenuCellDelegate {
    func showUserProfileButtonCliced(headerCell: HeaderMenuTableCell) {
        let parentController: DrawerViewController = self.parent as! DrawerViewController
        parentController.showUserProfile()
        parentController.setDrawerState(.closed, animated: true)
    }
}
