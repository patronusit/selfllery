//
//  DrawerViewController.swift
//  Oras
//
//  Created by Ivan Zagorulko on 28.12.16.
//  Copyright © 2016 Patronus IT. All rights reserved.
//

import UIKit
import KYDrawerController

class DrawerViewController: KYDrawerController {
    
    let reposytoryManager = SLRepository.sharedRepository

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.drawerWidth = UIScreen.main.bounds.size.width * 0.85
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func showUserProfile() {
        self.performSegue(withIdentifier: "showUserProfile", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showUserProfile" {
            let userProfileController = (segue.destination as! UINavigationController).topViewController as! UserProfileViewController
            userProfileController.viewModel.userProfileId.set(value: self.reposytoryManager.currentUserID.get)
            userProfileController.viewModel.isOwnProfile.set(value: true)
        }
    }

}
