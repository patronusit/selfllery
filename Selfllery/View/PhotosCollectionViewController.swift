//
//  PhotosCollectionViewController.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 15.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class PhotosCollectionViewController: ViewController {
    
    @IBOutlet weak var backBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    let viewModel = PhotosCollectionViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.viewModel.photosGaleryName.get
        
        self.viewModel.backCommand <- self.backBarButtonItem
        
        self.viewModel.backCommand.subscribe { [unowned self] _ in
            self.navigationController?.popViewController(animated: true)
        }
        
        self.viewModel.galeryPhotos.change.subscribe(onNext: { _ in
            self.photoCollectionView.reloadData()
        })
        
        self.viewModel.loadGaleryPhotosCommand.execute()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.loadGaleryPhotosCommand.execute()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showPhotoDetail" {
            let photoDetailController = segue.destination as! PhotoDetailViewController
            photoDetailController.viewModel.photoDetail.set(value: self.viewModel.selectPhoto.get)
        }
    }
}

extension PhotosCollectionViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.bounds.maxY == scrollView.contentSize.height) {
            self.viewModel.loadGaleryPage.set(value: self.viewModel.loadGaleryPage.get + 1)
            self.viewModel.loadGaleryPhotosCommand.execute()
        }
    }
}

extension PhotosCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.galeryPhotos.get.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = self.viewModel.galeryPhotos.get[indexPath.row]
        self.viewModel.selectPhoto.set(value: photo)
        self.performSegue(withIdentifier: "showPhotoDetail", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let usableSpace = collectionView.frame.width - 2 * collectionViewContentInset - 2 * collectionViewCellContentInset
        let cellWidth: CGFloat = usableSpace / 3.0
        let cellHeight: CGFloat = usableSpace / 3.0
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCollectionCell
        let photo = self.viewModel.galeryPhotos.get[indexPath.row]
        cell.setSmallPhotoImage(photoUrl: photo.photoUrl)
        return cell
    }
}
