//
//  SLTextField.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 08.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material

@IBDesignable
class SLTextField: TextField {
//
//    @IBInspectable
//    open var leftImage: UIImage? {
//        didSet {
//            self.leftViewMode = .always
//            let imageView = UIImageView()
//            imageView.image = leftImage
//            self.leftView = imageView
//        }
//    }
//
//    @IBInspectable
//    var placeholderColor: UIColor = UIColor.white {
//        didSet {
//            guard let placeholder = attributedPlaceholder else { return }
//            attributedPlaceholder = NSAttributedString(string: placeholder.string, attributes: [NSForegroundColorAttributeName: placeholderColor])
//        }
//    }
    
//    override func prepare() {
//        super.prepare()
//        dividerContentEdgeInsets.left = 0.0
//        leftViewOffset = 0.0
//        textColor = UIColor(red: 118.0/255.0, green: 118.0/255.0, blue: 118.0/255.0, alpha: 1.0)
//    }
    
    func prepareForLoginView() {
        backgroundColor = UIColor(red: 230.0/255.0, green: 228.0/255.0, blue: 228.0/255.0, alpha: 1.0)
        textColor = UIColor(red: 118.0/255.0, green: 118.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        self.layer.sublayerTransform = CATransform3DMakeTranslation(16, 0, 0)
    }
    
//    @IBInspectable var SLpaddingLeft: CGFloat {
//        get {
//            return leftView!.frame.size.width
//        }
//        set {
//            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
//            leftView = paddingView
//            leftViewMode = .always
//        }
//    }
    
//    @IBInspectable var SLpaddingRight: CGFloat {
//        get {
//            return rightView!.frame.size.width
//        }
//        set {
//            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
//            rightView = paddingView
//            rightViewMode = .always
//        }
//    }

}


