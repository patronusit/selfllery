//
//  SLButton.swift
//  Selfllery
//
//  Created by victor on 08.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import Material

@IBDesignable
class SLButton: Button {
    
    func prepareForLoginView() {
        backgroundColor = UIColor(hex: 0xea5050)
        titleColor = UIColor(hex: 0xffffff)
        
    }
    
    func prepareFBButton()  {
        backgroundColor = UIColor(hex: 0x325b99)
        titleColor = UIColor(hex: 0xffffff)
    }
    
    func prepareGoogleButton()  {
        backgroundColor = UIColor(hex: 0xe54f34)
        titleColor = UIColor(hex: 0xffffff)
    }
}

