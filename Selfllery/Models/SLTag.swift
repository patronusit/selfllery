//
//  SLTag.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 20.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLTag: Object, Mappable {
    
    dynamic var tagID = 0
    dynamic var tagText: String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "tagID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["tag_id"].isKeyPresent == true else {
            return
        }
        tagID <- (map["tag_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        tagText <- map["tag_text"]
    }
    
}
