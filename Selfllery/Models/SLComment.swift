//
//  SLComment.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 08.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLComment: Object, Mappable {
    
    dynamic var commentID = 0
    dynamic var parentId = 0
    dynamic var photoId = 0
    dynamic var authorId = 0
    dynamic var level = 0
    dynamic var commentText: String?
    dynamic var date: Date?
    dynamic var commentDate: Date?
    dynamic var fullName: String?
    dynamic var avatarUrl: String?
    dynamic var childCount = 0
    dynamic var usefulness = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "commentID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["comment_id"].isKeyPresent == true else {
            return
        }
        commentID <- (map["comment_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        parentId <- map["parent_id"]
        photoId <- map["photo_id"]
        authorId <- map["author_id"]
        level <- map["level"]
        commentText <- map["text"]
        date <- (map["date"], DateFormatTransform())
        commentDate <- (map["comment_date"], DateFormatTransform())
        avatarUrl <- map["avatar_url"]
        fullName <- map["full_name"]
        childCount <- map["child_count"]
        usefulness <- map["usefulness"]
    }
    
    func commentDateString() -> String {
        guard self.commentDate != nil else { return "" }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM.dd.yyyy"
        let dateString = dateFormatter.string(from: self.commentDate!)
        return dateString
    }
}
