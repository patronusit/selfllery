//
//  SLEvent.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 12.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLEvent: Object, Mappable {
    
    dynamic var eventID = 0
    dynamic var eventDate: NSDate?

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "eventID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        eventID <- (map["event_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        
    }
    
}
