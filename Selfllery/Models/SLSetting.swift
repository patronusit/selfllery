//
//  SLSetting.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 28.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLSetting: Object, Mappable {
    
    dynamic var charityCategoryID = 0
    dynamic var charityState = false
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "charityCategoryID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["charity_category_id"].isKeyPresent == true else {
            return
        }
        charityCategoryID <- (map["charity_category_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        charityState <- map["enabled"]
    }
    
}
