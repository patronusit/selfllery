//
//  SLCategory.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 08.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLCategory: Object, Mappable {
    
    dynamic var categoryID = 0
    dynamic var categoryTitle: String = ""
    dynamic var totalPhotos = 0
    dynamic var hourPhotos = 0
    dynamic var backgroundUrl: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "categoryID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["group_id"].isKeyPresent == true else {
            return
        }
        categoryID <- (map["group_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        categoryTitle <- map["title"]
        totalPhotos <- map["total_photos"]
        hourPhotos <- map["hour_photos"]
        backgroundUrl <- map["background_img"]
    }
    
}
