//
//  SLPhoto.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 08.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLPhoto: Object, Mappable {
    
    dynamic var photoID = 0
    dynamic var authorId = 0
    dynamic var photoTitle: String = ""
    dynamic var photoDescr: String = ""
    dynamic var photoDate: Date?
    dynamic var photoAuthorName: String = ""
    dynamic var photoAuthorLastName: String = ""
    dynamic var photoAuthorFullName: String = ""
    dynamic var photoAuthorAvatarUrl: String?
    dynamic var photoUrl: String?
    dynamic var groupId = 0
    dynamic var countLikes = 0
    dynamic var countView = 0
    dynamic var isPopular = false
    dynamic var isEditorChoice = false
    dynamic var isLiked = false
    
    var tags = List<SLTag>()
    var comments = List<SLComment>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "photoID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["photo_id"].isKeyPresent == true else {
            return
        }
        photoID <- (map["photo_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        authorId <- (map["author_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        photoTitle <- map["title"]
        photoDescr <- map["description"]
        photoDate <- (map["date"], DateFormatTransform())
        photoUrl <- map["photo_url"]
        photoAuthorName <- map["name_first"]
        photoAuthorLastName <- map["name_last"]
        photoAuthorFullName <- map["full_name"]
        photoAuthorAvatarUrl <- map["avatar_url"]
        groupId <- (map["group_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        countLikes <- (map["count_likes"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        countView <- (map["count_view"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        isLiked <- map["is_liked"]
        
        tags <- (map["tags"], ArrayTransform<SLTag>())
    }
    
    func photoDateString() -> String {
        guard photoDate != nil else { return "" }
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        return dayTimePeriodFormatter.string(from: photoDate! as Date)
    }
}
