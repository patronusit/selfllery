//
//  SLCharity.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 28.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLCharity: Object, Mappable {
    
    dynamic var charityID = 0
    dynamic var charityText: String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "charityID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["charity_category_id"].isKeyPresent == true else {
            return
        }
        charityID <- (map["charity_category_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        charityText <- map["text"]
    }
    
}
