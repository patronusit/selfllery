//
//  SLCountry.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 19.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLCountry: Object, Mappable {
    
    dynamic var countryID = 1
    dynamic var countryName: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "countryID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["country_id"].isKeyPresent == true else {
            return
        }
        countryID <- (map["country_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        countryName <- map["country_name"]
    }
    
}
