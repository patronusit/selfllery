//
//  SLUser.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 05.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class SLUser: Object, Mappable {
    
    dynamic var userID = 0
    dynamic var firstName: String = ""
    dynamic var lastName: String = ""
    dynamic var fullName: String = ""
    dynamic var email: String = ""
    dynamic var birthDate: Date?
    dynamic var gender: String = ""
    dynamic var userToken: String?
    dynamic var avatarSmalUrl: String?
    dynamic var avatarUrl: String?
    dynamic var backgroundImg: String?
    dynamic var backgroundUrl: String?
    dynamic var countryId = 0
    dynamic var followersCount = 0
    dynamic var followingCount = 0
    dynamic var rating = 0
    dynamic var tokensCount = 0.0
    dynamic var countryName: String = ""
    dynamic var isFollow = false
    dynamic var totalPercent = 0
    
    var followers = List<SLUser>()
    var following = List<SLUser>()
    var profileComments = List<SLComment>()
    var userSettings = List<SLSetting>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //Set primary key from data base
    override static func primaryKey() -> String? {
        return "userID"
    }
    
    //Mapping data to model
    func mapping(map: Map) {
        guard map["author_id"].isKeyPresent == true else {
            return
        }
        userID <- (map["author_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        userToken <- map["user_token"]
        firstName <- map["name_first"]
        lastName <- map["name_last"]
        fullName <- map["full_name"]
        email <- map["email"]
        birthDate <- (map["birthday_date"], DateFormatTransform(dateFormat: "dd.MM.yyyy"))
        gender <- map["gender"]
        avatarSmalUrl <- map["avatar_small_url"]
        avatarUrl <- map["avatar_url"]
        backgroundImg <- map["background_img"]
        backgroundUrl <- map["background_url"]
        //countryId <- (map["country_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        followersCount <- (map["followers_count"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        followingCount <- (map["following_count"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        rating <- (map["rating"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        tokensCount <- (map["tokens_count"], TransformOf<Double, String>(fromJSON: { Double($0!) }, toJSON: { $0.map { String($0) } }))
        countryName <- map["country_name"]
    }
    
    func userBirthDate() -> String {
        guard self.birthDate != nil else { return "" }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let dateString = dateFormatter.string(from: self.birthDate!)
        return dateString
    }
    
    func getSettings() -> [[String: Any]] {
        var settings: [[String: Any]] = []
        let settingsArray = self.userSettings.toArray()
        for setting in settingsArray {
            let cSetting = ["category_id" : setting.charityCategoryID,
                            "category_enabled" : setting.charityState] as [String : Any]
            settings.append(cSetting)
        }
        return settings
    }
    
    func getGender() -> String {
        if self.gender == nil {
            return ""
        }
        return (self.gender == "0") ?"Male" :"Female"
    }
}

/*
 "author_id" = 101738;
 "avatar_small_url" = "https://selfllery.com/photo_auth/0-avatar_big_male.png";
 "avatar_url" = "https://selfllery.com/photo_auth/0-avatar_big_male.png";
 "background_img" = "<null>";
 "background_url" = "";
 "birthday_date" = "<null>";
 "country_id" = "<null>";
 email = "test@gmail.com";
 "followers_count" = 0;
 "following_count" = 0;
 "full_name" = "";
 gender = "<null>";
 "name_first" = "";
 "name_last" = "";
 rating = 0;
 "tokens_count" = "0.000";
 "user_token" = 7c6527ccd5b0e001c65485835279033b265722c3;
 */
