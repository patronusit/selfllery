//
//  ListingRequests.swift
//  Selfllery
//
//  Created by victor on 03.02.2018.
//  Copyright © 2018 selfllery. All rights reserved.
//

import Foundation
import Alamofire

public class ListingRequest {
    
    let kBaseUrlString: String = "https://selfllery.com/api"
    
    func feedListing(userToken: String, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "page" : page]
        return Alamofire.request("\(kBaseUrlString)/feed/listingr", method: .post, parameters: parametrs)
    }
    
    func categoryListing(userToken: String) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        return Alamofire.request("\(kBaseUrlString)/category/listing", method: .post, parameters: parametrs)
    }
    
    func photoListing(userToken: String, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "page" : page] 
        return Alamofire.request("\(kBaseUrlString)/photo/listing", method: .post, parameters: parametrs)
    }
}


