//
//  CharityRequests.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 28.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import Alamofire

public class CharityRequests {
    
    let kBaseUrlString: String = "https://selfllery.com/api/charity"
    
    func charityListing(userToken: String) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        return Alamofire.request("\(kBaseUrlString)/categories_listing", method: .post, parameters: parametrs)
    }
    
    func getUserSettings(userToken: String) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        return Alamofire.request("\(kBaseUrlString)/get_settings", method: .post, parameters: parametrs)
    }
    
    func setUserSettings(userToken: String, percent: String, settings: Parameters) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "percent" : percent,
                                     "settings" : settings]
        return Alamofire.request("\(kBaseUrlString)/set_settings", method: .post, parameters: parametrs)
    }
}
