//
//  PhotoRequests.swift
//  Selfllery
//
//  Created by victor on 03.02.2018.
//  Copyright © 2018 selfllery. All rights reserved.
//

import Foundation
import Alamofire

public class PhotoRequests {
    let kBaseUrlString: String = "https://selfllery.com/api/photo"
    
    func photoShow(userToken: String, photoId: Int, source: String) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "photo_id" : photoId,
                                     "source" : source]
        return Alamofire.request("\(kBaseUrlString)/show", method: .post, parameters: parametrs)
    }
    
    func photoUpload(userToken: String, image: UIImage, encodingComplection: ((Alamofire.SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let imageData = UIImageJPEGRepresentation(image, 1) {
                multipartFormData.append(imageData, withName: "upload", fileName: "user_photo", mimeType: "image/jpg")
            }
            for (key, value) in parametrs {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: "\(kBaseUrlString)/upload", headers: nil, encodingCompletion: encodingComplection)
    }
    
    func photoApprove(userToken: String, title: String, description: String, tags: [String], categoryId: Int, photoId: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "title" : title,
                                     "description" : description,
                                     "tags" : tags,
                                     "category_id" : categoryId,
                                     "photo_id" : photoId]
        return Alamofire.request("\(kBaseUrlString)/approve", method: .post, parameters: parametrs)
    }
    
    func photoComments(userToken: String, photoId: Int, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "photo_id" : photoId,
                                     "page" : page]
        return Alamofire.request("\(kBaseUrlString)/comments", method: .post, parameters: parametrs)
    }
    
    func photoCategory(userToken: String, categoryId: Int, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "category_id" : categoryId,
                                     "page" : page]
        return Alamofire.request("\(kBaseUrlString)/category", method: .post, parameters: parametrs)
    }
    
    func photoEditorsChoice(userToken: String, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "page" : page]
        return Alamofire.request("\(kBaseUrlString)/editors_choice", method: .post, parameters: parametrs)
    }
    
    func photoPopular(userToken: String, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "page" : page]
        return Alamofire.request("\(kBaseUrlString)/popular", method: .post, parameters: parametrs)
    }
    
    func photoLike(userToken: String, photoId: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "photo_id" : photoId]
        return Alamofire.request("\(kBaseUrlString)/like", method: .post, parameters: parametrs)
    }
    
    func complaintPhoto(userToken:String, photoId: Int, complaintPhotoText: String) -> DataRequest {
        let parametrs: Parameters = ["app_key": appKey,
                                     "user_token": userToken,
                                     "photo_id": photoId,
                                     "message": complaintPhotoText]
        return Alamofire.request("https://selfllery.com/api/complaint/photo", method: .post, parameters: parametrs)
    }
}
