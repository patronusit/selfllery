//
//  GalleryRequests.swift
//  Selfllery
//
//  Created by victor on 04.02.2018.
//  Copyright © 2018 selfllery. All rights reserved.
//

import Foundation
import Alamofire

public class GalleryRequests {
    
    let kBaseUrlString: String = "https://selfllery.com/api/gallery"
    
    func galleryUser(userToken: String, page: Int, authorId: Int = 0) -> DataRequest {
        var parametrs: Parameters = ["app_key": appKey,
                                     "user_token": userToken,
                                     "page" : page]
        if authorId != 0 {
            parametrs["author_id"] = authorId
        }
        return Alamofire.request("\(kBaseUrlString)/user", method: .post, parameters: parametrs)
    }
    
    func galleryComments(userToken: String, galleryId: Int, page: Int) -> DataRequest {
        let parametrs :Parameters = ["user_token" : userToken,
                                     "gallery_id" : galleryId,
                                     "page" : page] 
        return Alamofire.request("\(kBaseUrlString)/comments", method: .post, parameters: parametrs)
    }
}
