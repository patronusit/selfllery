//
//  CommentsRequests.swift
//  Selfllery
//
//  Created by victor on 04.02.2018.
//  Copyright © 2018 selfllery. All rights reserved.
//

import Foundation
import Alamofire

public class CommentsRequests {
    
    let kBaseUrlString: String = "https://selfllery.com/api/comments"
    
    func commentsPost(userToken:String, commentText:String, objectId: Int) -> DataRequest {
        let parametrs :Parameters = ["user_token" : userToken,
                                     "comment_text" : commentText,
                                     "object_id" : objectId]
        return Alamofire.request("\(kBaseUrlString)/post", method: .post, parameters: parametrs)
    }

    func commentsReply(userToken:String, commentText:String, commentType:String, objectId:Int, parentId:Int) -> DataRequest {
        let parametrs: Parameters = ["user_token": userToken,
                                      "comment_text": commentText,
                                      "comment_type": commentType,
                                      "object_id": objectId,
                                      "parent_id": parentId]
        
        return Alamofire.request("\(kBaseUrlString)/reply", method: .post, parameters: parametrs)
    }
    
    func commentsDelete(userToken:String, commentText:String, commentType:String) -> DataRequest {
        let parametrs: Parameters = ["user_token": userToken,
                                     "comment_text": commentText,
                                     "comment_type": commentType]
        return Alamofire.request("\(kBaseUrlString)/delete", method: .post, parameters: parametrs)
    }
    
    func commentsEdit(userToken:String, commentId: Int, commentText: String, commentType: Int) -> DataRequest {
        let parametrs: Parameters = ["user_token": userToken,
                                     "comment_id": commentId,
                                     "comment_text": commentText,
                                     "comment_type": commentType]
        return Alamofire.request("\(kBaseUrlString)/edit", method: .post, parameters: parametrs)
    }

    func commentsLike(userToken:String, commentId: Int, commentType: Int, isLike: Bool) -> DataRequest {
        let parametrs: Parameters = ["user_token": userToken,
                                     "comment_id": commentId,
                                     "comment_type": commentType,
                                     "is_like": isLike]
        return Alamofire.request("\(kBaseUrlString)/like", method: .post, parameters: parametrs)
    }

    func commentsIgnore(userToken:String, commentId: Int, commentType: Int) -> DataRequest {
        let parametrs: Parameters = ["user_token": userToken,
                                     "comment_id": commentId,
                                     "comment_type": commentType] 
        return Alamofire.request("\(kBaseUrlString)/ignore", method: .post, parameters: parametrs)
    }
    
    func complaintComments(userToken:String, commentId: Int, complaintCommentText: String) -> DataRequest {
        let parametrs: Parameters = ["user_token": userToken,
                                     "comment_id": commentId,
                                     "message": complaintCommentText]
        return Alamofire.request("https://selfllery.com/api/complaint/comment", method: .post, parameters: parametrs)
    }
}
