//
//  ProfileRequests.swift
//  Selfllery
//
//  Created by victor on 02.02.2018.
//  Copyright © 2018 selfllery. All rights reserved.
//

import Foundation
// Token - f8fb308b64e78c253a1a4fb5cbc0b08281289654
import Alamofire

//let kBaseUrlString: String = "https://selfllery.com/api"
let appKey: String = "8r1tugTeUN5inhct9gOmpDB46CTgGCTP"

public class ProfileRequests {
    let kBaseUrlString: String = "https://selfllery.com/api/user"
    
    func userRegister(email: String, password: String, language: String) -> DataRequest {
        let parametrs: Parameters = ["app_key": appKey,
                                     "email": email,
                                     "password": password,
                                     "language": language]
        return Alamofire.request("\(kBaseUrlString)/register", method: .post, parameters: parametrs)
    }
    
    func userAuth(authType: String, email: String, password: String, language: String) -> DataRequest {
        let parametrs: Parameters = ["app_key": appKey,
                                     "auth_type": authType,
                                     "email": email,
                                     "password": password,
                                     "language": language]
        return Alamofire.request("\(kBaseUrlString)/auth", method: .post, parameters: parametrs)
    }
    
    func userAuthBySocial(authType: String, socialToken: String, language: String) -> DataRequest {
        let parametrs: Parameters = ["app_key": appKey,
                                     "auth_type": authType,
                                     "social_token": socialToken,
                                     "language": language]
        return Alamofire.request("\(kBaseUrlString)/auth", method: .post, parameters: parametrs)
    }
    
    func getuUserProfile(userToken: String, authorId: Int = 0) -> DataRequest {
        var parametrs: Parameters = ["app_key": appKey,
                                     "user_token": userToken]
        if authorId != 0 {
            parametrs["author_id"] = authorId
        }
        return Alamofire.request("\(kBaseUrlString)/profile", method: .post, parameters: parametrs)
    }
    
    func updateUserProfile(userToken: String, nameFirst: String, nameLast: String, email: String, language: String, countryId: Int, gender: String, birthdayDate: String) -> DataRequest {
        let parametrs: Parameters = ["app_key": appKey,
                                     "user_token": userToken,
                                     "name_first": nameFirst,
                                     "name_last": nameLast,
                                     "email": email,
                                     "language": language,
                                     "country_id": countryId,
                                     "gender": (gender == "Male") ?"0" :"1",
                                     "birthday_date": birthdayDate]
       return Alamofire.request("\(kBaseUrlString)/profile", method: .post, parameters: parametrs)
    }
    
    func validateUserToken(userToken: String, authorId: Int) -> DataRequest {
        let parametrs: Parameters = ["app_key": appKey,
                                     "user_token": userToken,
                                     "author_id": authorId]
        return Alamofire.request("\(kBaseUrlString)/token_verify", method: .post, parameters: parametrs)
    }
    
    func setUserAvatar(userToken: String, avatarImage: UIImage, encodingComplection: ((Alamofire.SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let imageData = UIImageJPEGRepresentation(avatarImage, 1) {
                multipartFormData.append(imageData, withName: "upload", fileName: "user_avatar", mimeType: "image/jpg")
            }
            
            for (key, value) in parametrs {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: "\(kBaseUrlString)/set_avatar", headers: nil, encodingCompletion: encodingComplection)
    }
    
    func setUserBackground(userToken: String, backgroundImage: UIImage, encodingComplection: ((Alamofire.SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let imageData = UIImageJPEGRepresentation(backgroundImage, 1) {
                multipartFormData.append(imageData, withName: "upload", fileName: "user_background", mimeType: "image/jpg")
            }
            
            for (key, value) in parametrs {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: "\(kBaseUrlString)/set_background", headers: nil, encodingCompletion: encodingComplection)
    }
    
    func userFollow(userToken: String, authorId: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "author_id": authorId]
        return Alamofire.request("\(kBaseUrlString)/follow", method: .post, parameters: parametrs)
    }
    
    func userFollowing(userToken: String, authorId: Int, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "author_id": authorId,
                                     "page": page]
        return Alamofire.request("\(kBaseUrlString)/following", method: .post, parameters: parametrs)
    }
    
    func userFollowers(userToken: String, authorId: Int, page: Int) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken,
                                     "author_id": authorId,
                                     "page": page]
        return Alamofire.request("\(kBaseUrlString)/followers", method: .post, parameters: parametrs)
    }
    
    func userDeleteAvatar(userToken: String) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        return Alamofire.request("\(kBaseUrlString)/delete_avatar", method: .post, parameters: parametrs)
    }
    
    func userDeleteBackground(userToken: String) -> DataRequest {
        let parametrs :Parameters = ["app_key": appKey,
                                     "user_token" : userToken]
        return Alamofire.request("\(kBaseUrlString)/delete_background", method: .post, parameters: parametrs)
    }
    
    func countriesListing(userToken: String) -> DataRequest {
        let parametrs :Parameters = ["user_token" : userToken]
        return Alamofire.request("https://selfllery.com/api/system/countries_listing", method: .post, parameters: parametrs)
    }
    
    func ignoreUser(userToken:String, ignorableUserId: Int) -> DataRequest {
        let parametrs :Parameters = ["user_token" : userToken,
                                     "user_id" : ignorableUserId]
        return Alamofire.request("\(kBaseUrlString)/ignore", method: .post, parameters: parametrs)
    }
}
//system/countries_listing
/*
 "author_id" = 101738;
 "avatar_small_url" = "https://selfllery.com/photo_auth/0-avatar_big_male.png";
 "avatar_url" = "https://selfllery.com/photo_auth/0-avatar_big_male.png";
 "background_img" = "<null>";
 "background_url" = "";
 "birthday_date" = "<null>";
 "country_id" = "<null>";
 email = "test@gmail.com";
 "followers_count" = 0;
 "following_count" = 0;
 "full_name" = "";
 gender = "<null>";
 "name_first" = "";
 "name_last" = "";
 rating = 0;
 "tokens_count" = "0.000";
 "user_token" = 7c6527ccd5b0e001c65485835279033b265722c3;
 */
