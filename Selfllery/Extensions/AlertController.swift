//
//  AlertController.swift
//  Selfllery
//
//  Created by victor on 28.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//


import UIKit

extension UIAlertController {
    class func customAlertController(title : String, message : String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) 
        alertController.addAction(OKAction)
        return alertController
    }
    
    class func settingsAllert() -> UIAlertController {
        let alertController = UIAlertController (title: "Need camera access", message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    //print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
}
