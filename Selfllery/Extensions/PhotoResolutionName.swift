//
//  PhotoResolutionName.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 14.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation

public enum PhotoResolutionName: String {
    case square650 = "_s_650"
    case square320 = "_s_320"
    case square150 = "_s_150"
    case align320 = "_a_320"
    case wide320 = "_w_320"
}
