//
//  UIScrollView+Additions.swift
//  Dolly
//
//  Created by Ivan Zagorulko on 01.03.16.
//  Copyright © 2016 Norse Digital. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    func stopListenningForKeyboardNotification() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func startListeningForKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(UIScrollView.keyboardWillShow(note:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UIScrollView.keyboardWillHide(note:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(note: NSNotification) {
        if let duration = note.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval, let bounds = note.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            UIView.animate(withDuration: duration) { _ in
                self.contentInset = UIEdgeInsetsMake(0, 0, bounds.cgRectValue.height, 0)
                self.scrollIndicatorInsets = self.contentInset
            }
        }
    }
    
    func keyboardWillHide(note: NSNotification) {
        if let duration = note.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval {
            UIView.animate(withDuration: duration) { _ in
                self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                self.scrollIndicatorInsets = self.contentInset
            }
        }
    }
}
