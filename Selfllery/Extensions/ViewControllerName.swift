//
//  ViewControllerName.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 09.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation

public enum ViewControllerName: String {
    case HomeViewControllerName = "HomeViewController"
    case UploadPhotosViewControllerName = "UploadPhotosViewController"
    case EditProfileViewControllerName = "EditProfileViewController"
    case PhotosViewControllerName = "PhotosViewController"
    case AboutViewControllerName = "AboutViewController"
}
