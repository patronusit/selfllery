//
//  SLRepository.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 05.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import RxSwift
import RealmSwift
import ObjectMapper
import SwiftyJSON

let kSLRepositoryName: String = "selflleryApp.realm"

extension Results {
    
    func toArray() -> [Element] {
        return self.map{$0}
    }
}

extension RealmSwift.List {
    
    func toArray() -> [Element] {
        return self.map{$0}
    }
}

class SLRepository {
    
    static let sharedRepository = SLRepository()
    
    let netService = SLNetworkService.sharedService
    
    let currentUserID: Property<Int> = Property(0)
    
    init() {
        self.configurateRealmDataBase()
    }
    
    // Configurate Realm Data Base
    func configurateRealmDataBase() {
        var config = Realm.Configuration()
        config.schemaVersion = 1
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent(kSLRepositoryName)
        config.migrationBlock = { migration, oldSchemaVersion in
        }
        Realm.Configuration.defaultConfiguration = config
    }
    
    lazy var countriesListingCommand:AsyncCommand<Bool> = AsyncCommand {
        return self.loadCountries()
    }
    
    lazy var charityListingCommand:AsyncCommand<Bool> = AsyncCommand {
        return self.loadCharities()
    }
    
    lazy var photoCategoriesCommand:AsyncCommand<Bool> = AsyncCommand {
        return self.loadPhotoCategory()
    }
    
    func loadCategoryOfPhoto() {
        self.photoCategoriesCommand.execute()
    }
    
    func loadCountriesListing() {
        self.countriesListingCommand.execute()
    }
    
    func loadCharityListing() {
        self.charityListingCommand.execute()
    }
    
    func loadUserProfile(userId: Int) -> Observable<SLUser?> {
        return self.netService.userProfile(authorId: (userId == self.currentUserID.get) ?0 :userId).flatMap({ userProfile -> Observable<SLUser?> in
            guard let profileJson = userProfile as? AnyObject else { return  Observable.just(nil) }
            self.saveUserInfo(data: profileJson)
            let realm = try! Realm()
            return Observable.just(realm.objects(SLUser.self).filter("userID == %d", userId).toArray().first)
        })
    }
    
    func loadUserPhoto(page: Int, userId: Int = 0) -> Observable<[SLPhoto]> {
        return self.netService.galleryUser(page: page, authorId: userId).flatMap({ photos -> Observable<[SLPhoto]> in
            guard let photosJson = photos as? AnyObject else { return  Observable.just([]) }
            self.clearDeletedPhoto(page: page, userId: userId, data: photosJson)
            self.savePhotos(data: photosJson)
            let realm = try! Realm()
            return Observable.just(realm.objects(SLPhoto.self).filter("authorId == %d", (userId == 0 ?self.currentUserID.get :userId)).sorted(byKeyPath: "photoID", ascending: false).toArray())
        })
    }
    
    func loadUserFollowers(page: Int, userId: Int) -> Observable<[SLUser]> {
        return self.netService.userFollowers(authorId: userId, page: page).flatMap({ usersInfo -> Observable<[SLUser]> in
            guard let usersJson = usersInfo as? AnyObject else { return  Observable.just([]) }
            self.saveUserFollowers(userId: userId, usersData: usersJson)
            return Observable.just(self.getUser(userID: userId)!.followers.toArray())
        })
    }
    
    func loadUserFollowing(page: Int, userId: Int) -> Observable<[SLUser]> {
        return self.netService.userFollowing(authorId: userId, page: page).flatMap({ usersInfo -> Observable<[SLUser]> in
            guard let usersJson = usersInfo as? AnyObject else { return  Observable.just([]) }
            self.saveUserFollowing(userId: userId, usersData: usersJson)
            return Observable.just(self.getUser(userID: userId)!.following.toArray())
        })
    }
    
    func loadUserComments(userId: Int, page: Int) -> Observable<[SLComment]> {
        return self.netService.galleryComments(galleryId: userId, page: page).flatMap { comments -> Observable<[SLComment]> in
            guard let commentsJson =  comments as? AnyObject else { return  Observable.just([]) }
            self.saveUserComments(userId: userId, data: commentsJson)
            return Observable.just(self.getUser(userID: userId)!.profileComments.toArray())
        }
    }
    
    func loadUserSettings() -> Observable<[SLSetting]> {
        return self.netService.getUserSettings().flatMap { charities -> Observable<[SLSetting]> in
            guard let charitiesJson =  charities as? AnyObject else { return  Observable.just([]) }
            self.saveUserSettings(data: charitiesJson)
            return Observable.just(self.getUser()!.userSettings.toArray())
        }
    }
    
    func charityUser(userSettings: [[String: Any]], totalPercent: String) -> Observable<Bool> {
        return self.netService.setUserSettings(percent: totalPercent, settings: userSettings).flatMap({ success -> Observable<Bool> in
            guard let successJson = success as? AnyObject else { return  Observable.just(false) }
            return Observable.just(true)
        })
    }
    
    func updateUserProfile(nameFirst: String, nameLast: String, email: String, countryId: Int, gender: String, birthdayDate: String) -> Observable<Bool> {
        return self.netService.updateUserProfile(nameFirst: nameFirst, nameLast: nameLast, email: email, countryId: countryId, gender: gender, birthdayDate: birthdayDate).flatMap({ userProfile -> Observable<Bool> in
            guard let profileJson = userProfile as? AnyObject else { return  Observable.just(false) }
            self.save(userData: profileJson)
            return Observable.just(true)
        })
    }
    
    func uploadAvatar(avatarImage: UIImage) -> Observable<Bool> {
        return self.netService.uploadUserAvatar(image: avatarImage).flatMap({ success -> Observable<Bool> in
            guard let successJson = success as? AnyObject else { return  Observable.just(false) }
            self.saveAvatar(data: successJson)
            return Observable.just(true)
        })
    }
    
    func uploadBackground(backgroundImage: UIImage) -> Observable<Bool> {
        return self.netService.uploadUserBackground(image: backgroundImage).flatMap({ success -> Observable<Bool> in
            guard let successJson = success as? AnyObject else { return  Observable.just(false) }
            self.saveBackground(data: successJson)
            return Observable.just(true)
        })
    }
    
    func loadPopularPhotos(page: Int) -> Observable<[SLPhoto]> {
        return self.netService.photoPopular(page: page).flatMap({ photos -> Observable<[SLPhoto]> in
            guard let photosJson =  photos as? AnyObject else { return  Observable.just([]) }
            self.savePhotos(data: photosJson, isPopular: true)
            let realm = try! Realm()
            return Observable.just(realm.objects(SLPhoto.self).filter("isPopular == true").sorted(byKeyPath: "countLikes", ascending: false).toArray())
        })
    }
    
    func loadEditorChoicePhotos(page: Int) -> Observable<[SLPhoto]> {
        return self.netService.photoEditorsChoice(page: page).flatMap({ photos -> Observable<[SLPhoto]> in
            guard let photosJson =  photos as? AnyObject else { return  Observable.just([]) }
            self.savePhotos(data: photosJson, isPopular: false, isEditorChoice: true)
            let realm = try! Realm()
            return Observable.just(realm.objects(SLPhoto.self).filter("isEditorChoice == true").sorted(byKeyPath: "photoDate", ascending: false).toArray())
        })
    }
    
    func loadGaleryPhotos(categoryId: Int, page: Int) -> Observable<[SLPhoto]> {
        return self.netService.photoCategory(categoryId: categoryId, page: page).flatMap({ photos -> Observable<[SLPhoto]> in
            guard let photosJson =  photos as? AnyObject else { return  Observable.just([]) }
            self.savePhotos(data: photosJson)
            let realm = try! Realm()
            return Observable.just(realm.objects(SLPhoto.self).filter("groupId == %d", categoryId).sorted(byKeyPath: "photoID", ascending: false).toArray())
        })
    }
    
    func loadPhotoComments(photoId: Int, page: Int) -> Observable<[SLComment]> {
        return self.netService.photoComments(photoId: photoId, page: page).flatMap { comments -> Observable<[SLComment]> in
            guard let commentsJson =  comments as? AnyObject else { return  Observable.just([]) }
            self.savePhotoComments(photoId: photoId, data: commentsJson)
            return Observable.just(self.getPhoto(photoID: photoId)!.comments.toArray())
        }
    }
    
    func loadPhotoCategory() -> Observable<Bool> {
        return self.netService.categoryListing().flatMap({ category -> Observable<Bool> in
            guard let categoryJson =  category as? AnyObject else { return  Observable.just(false) }
            self.saveCategorie(data: categoryJson)
            return Observable.just(true)
        })
    }
    
    func loadCountries() -> Observable<Bool> {
        return self.netService.countriesListing().flatMap({ country -> Observable<Bool> in
            guard let countryJson =  country as? AnyObject else { return  Observable.just(false) }
            self.saveCoutries(data: countryJson)
            return Observable.just(true)
        })
    }
    
    func loadCharities() -> Observable<Bool> {
        return self.netService.charityListing().flatMap({ charity -> Observable<Bool> in
            guard let charityJson =  charity as? AnyObject else { return  Observable.just(false) }
            self.saveCharities(data: charityJson)
            return Observable.just(true)
        })
    }
    
    func likePhoto(photoId: Int) -> Observable<Bool> {
        return self.netService.photoLike(photoId: photoId).flatMap({ success -> Observable<Bool> in
            guard let successJson = success as? AnyObject else { return  Observable.just(false) }
            self.savePhotoLike(photoId: photoId, data: successJson)
            return Observable.just(true)
        })
    }
    
    func followUser(userId: Int) -> Observable<Bool> {
        return self.netService.userFollow(authorId: userId).flatMap({ success -> Observable<Bool> in
            guard let successJson = success as? AnyObject else { return  Observable.just(false) }
            self.saveFollowBy(userId: userId, data: successJson)
            return Observable.just(true)
        })
    }
    
    func savePhotoLike(photoId: Int, data: AnyObject) {
        let photoJson = data.value(forKeyPath: "count_likes") as! String
        let photoInfo = self.getPhoto(photoID: photoId)!
        let realm = try! Realm()
        try! realm.write({
            photoInfo.isLiked = true
            photoInfo.countLikes = Int(photoJson)!
        })
    }
    
    func saveFollowBy(userId: Int, data: AnyObject) {
        let isFollowJson = data.value(forKeyPath: "is_i_follow") as? AnyObject
        let userProfile = self.getUser(userID: userId)!
        let currentUser = self.getUser()!
        let isFollow = isFollowJson!.boolValue
        let realm = try! Realm()
        try! realm.write({
            userProfile.isFollow = isFollow!
            if isFollow! {
                userProfile.followersCount += 1
            } else {
                userProfile.followersCount -= 1
                if let posUser = userProfile.followers.index(of: currentUser) {
                    userProfile.followers.remove(at: posUser)
                }
            }
        })
    }
    
    func save(userData: AnyObject) {
        guard let userJson =  userData.value(forKeyPath: "data") as? AnyObject else { return }
        print(userJson)
        let realm = try! Realm()
        if let user = Mapper<SLUser>().map(JSONObject: userJson) {
            try! realm.write({
                realm.create(SLUser.self, value: user, update: true)
            })
            self.currentUserID.set(value: user.userID)
            self.netService.currentUser.set(value: user)
            if let userToken = user.userToken {
                self.netService.userToken.set(value: userToken)
            }
        }
    }
    
    func saveAvatar(data: AnyObject) {
        guard let userJson = data.value(forKeyPath: "data") as? AnyObject else { return }
        let avatarUrl = userJson.value(forKeyPath: "avatar_url") as! String
        let currentUser = self.getUser()!
        let realm = try! Realm()
        try! realm.write({
            currentUser.avatarSmalUrl = avatarUrl
        })
        self.netService.currentUser.set(value: currentUser)
    }
    
    func saveBackground(data: AnyObject) {
        guard let userJson = data.value(forKeyPath: "data") as? AnyObject else { return }
        let backgroundUrl = userJson.value(forKeyPath: "background_url") as! String
        let currentUser = self.getUser()!
        let realm = try! Realm()
        try! realm.write({
            currentUser.backgroundUrl = backgroundUrl
        })
        self.netService.currentUser.set(value: currentUser)
    }
    
    func saveUserInfo(data: AnyObject) {
        guard let userJson = data.value(forKeyPath: "data") as? AnyObject else { return }
        let isFollowJson = data.value(forKeyPath: "is_i_follow") as? AnyObject
        let realm = try! Realm()
        if let user = Mapper<SLUser>().map(JSONObject: userJson) {
            try! realm.write({
                realm.add(user, update: true)
                user.isFollow = isFollowJson!.boolValue
            })
        }
    }
    
    func saveUserFollowers(userId: Int, usersData: AnyObject) {
        guard let usersJson = usersData.value(forKeyPath: "data") as? AnyObject else { return }
        let userProfile = self.getUser(userID: userId)!
        let realm = try! Realm()
        if let users = Mapper<SLUser>().mapArray(JSONObject: usersJson) {
            try! realm.write({
                for user in users {
                   // if user.userID != self.currentUserID.get {
                        realm.add(user, update: true)
                        if userProfile.followers.index(of: user) == nil {
                            userProfile.followers.append(user)
                        }
                    //}
                }
            })
        }
    }
    
    func saveUserFollowing(userId: Int, usersData: AnyObject) {
        guard let usersJson = usersData.value(forKeyPath: "data") as? AnyObject else { return }
        let userProfile = self.getUser(userID: userId)!
        let realm = try! Realm()
        if let users = Mapper<SLUser>().mapArray(JSONObject: usersJson) {
            try! realm.write({
                for user in users {
                   // if user.userID != self.currentUserID.get {
                        realm.add(user, update: true)
                        if userProfile.following.index(of: user) == nil {
                            userProfile.following.append(user)
                        }
                   // }
                }
            })
        }
    }
    
    func saveUserComments(userId: Int, data: AnyObject) {
        guard let commentsJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let userProfile = self.getUser(userID: userId)!
        let realm = try! Realm()
        let comments = Mapper<SLComment>().mapArray(JSONObject: commentsJson)
        try! realm.write({
            for comment in comments! {
                realm.add(comment, update: true)
                if userProfile.profileComments.index(of: comment) == nil {
                    userProfile.profileComments.append(comment)
                }
            }
        })
    }
    
    func saveUserSettings(data: AnyObject) {
        guard let userSettingsJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let userProfile = self.getUser()!
        let settingsJson = userSettingsJson.value(forKeyPath: "settings")
        let charityPercent = userSettingsJson.value(forKeyPath: "percent") as? String
        let realm = try! Realm()
        let settings = Mapper<SLSetting>().mapArray(JSONObject: settingsJson)
        try! realm.write({
            for setting in settings! {
                realm.add(setting, update: true)
                if userProfile.userSettings.index(of: setting) == nil {
                    userProfile.userSettings.append(setting)
                }
            }
            userProfile.totalPercent = Int(charityPercent!)!
        })
        self.netService.currentUser.set(value: userProfile)
    }
    
    func clearDeletedPhoto(page: Int, userId: Int, data: AnyObject) {
        guard let photoJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let realm = try! Realm()
        let json = JSON(photoJson).arrayValue
        let startInd = page * 24
        var endInd = startInd + 23
        let photoIds = json.map { $0["photo_id"].intValue }
        let savedPhotos = self.getPhotos(authorID: (userId == 0 ?self.currentUserID.get :userId))
        let savedPhotosIds = savedPhotos.map { $0.photoID }
        if savedPhotosIds.count > 0 && savedPhotosIds.count - 1 > endInd {
            if endInd > savedPhotosIds.count {
                endInd = savedPhotosIds.count - 1
            }
            let subArray = savedPhotosIds[startInd...endInd]
            let deletePhotosIds = savedPhotosIds.filter({ !photoIds.contains($0) })
            if deletePhotosIds.count > 0 {
                let deletePhotos = savedPhotos.filter({ deletePhotosIds.contains($0.photoID) })
                try! realm.write({
                    //Delete old photo
                    realm.delete(deletePhotos)
                })
            }
        }
    }
    
    func savePhotos(data: AnyObject, isPopular: Bool = false, isEditorChoice: Bool = false) {
        guard let photoJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let realm = try! Realm()
        let photos = Mapper<SLPhoto>().mapArray(JSONObject: photoJson)
        try! realm.write({
            for photo in photos! {
                photo.isPopular = isPopular
                photo.isEditorChoice = isEditorChoice
                realm.create(SLPhoto.self, value: photo, update: true)
            }
        })
    }
    
    func savePhotoComments(photoId: Int, data: AnyObject) {
        guard let commentsJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let photoInfo = self.getPhoto(photoID: photoId)!
        let realm = try! Realm()
        let comments = Mapper<SLComment>().mapArray(JSONObject: commentsJson)
        try! realm.write({
            for comment in comments! {
                realm.add(comment, update: true)
                photoInfo.comments.append(comment)
            }
        })
    }
    
    func saveComments(data: AnyObject) {
        guard let commentsJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let realm = try! Realm()
        let comments = Mapper<SLComment>().mapArray(JSONObject: commentsJson)
        try! realm.write({
            for comment in comments! {
                realm.create(SLComment.self, value: comment, update: true)
            }
        })
    }
    
    func saveCategorie(data: AnyObject) {
        guard let categoriesJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let realm = try! Realm()
        let categories = Mapper<SLCategory>().mapArray(JSONObject: categoriesJson)
        try! realm.write({
            for category in categories! {
                realm.create(SLCategory.self, value: category, update: true)
            }
        })
    }
    
    func saveCoutries(data: AnyObject) {
        guard let countriesJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let realm = try! Realm()
        let countries = Mapper<SLCountry>().mapArray(JSONObject: countriesJson)
        try! realm.write({
            for country in countries! {
                realm.create(SLCountry.self, value: country, update: true)
            }
        })
    }
    
    func saveCharities(data: AnyObject) {
        guard let charitiesJson =  data.value(forKeyPath: "data") as? AnyObject else { return }
        let realm = try! Realm()
        let charities = Mapper<SLCharity>().mapArray(JSONObject: charitiesJson)
        try! realm.write({
            for charity in charities! {
                realm.create(SLCharity.self, value: charity, update: true)
            }
        })
    }
    
    func checkSetting(setting: SLSetting) {
        let realm = try! Realm()
        try! realm.write({
            setting.charityState = !setting.charityState
        })
        let userProfile = self.getUser()!
        self.netService.currentUser.set(value: userProfile)
    }
    
    func getUser(userID: Int = 0) -> SLUser? {
        let result = try! Realm().objects(SLUser.self).filter("userID == %d", (userID == 0 ?self.currentUserID.get :userID))
        guard result.count > 0 else { return nil }
        return result.first
    }
    
    func getPhotos(authorID: Int) -> [SLPhoto] {
        let result = try! Realm().objects(SLPhoto.self).filter("authorId == %d", authorID).sorted(byKeyPath: "photoID").toArray()
        guard result.count > 0 else { return [] }
        return result
    }
    
    func getPhoto(photoID: Int) -> SLPhoto? {
        let result = try! Realm().objects(SLPhoto.self).filter("photoID == %d", photoID).toArray()
        guard result.count > 0 else { return nil }
        return result.first
    }
    
    func getCategories() -> [SLCategory] {
        let result = try! Realm().objects(SLCategory.self).sorted(byKeyPath: "categoryID").toArray()
        guard result.count > 0 else { return [] }
        return result
    }
    
    func getCategory(byId: Int) -> SLCategory? {
        let result = try! Realm().objects(SLCategory.self).filter("categoryID == %d", byId)
        guard result.count > 0 else { return nil }
        return result.first
    }
    
    func getCharity(byId: Int) -> SLCharity? {
        let result = try! Realm().objects(SLCharity.self).filter("charityID == %d", byId)
        guard result.count > 0 else { return nil }
        return result.first
    }
    
    func getComments(photoID: Int) -> [SLComment] {
        let result = try! Realm().objects(SLComment.self).filter("photoId == %d", photoID).sorted(byKeyPath: "commentID").toArray()
        guard result.count > 0 else { return [] }
        return result
    }
    
    func getCountries() -> [SLCountry] {
        let result = try! Realm().objects(SLCountry.self).sorted(byKeyPath: "countryID").toArray()
        guard result.count > 0 else { return [] }
        return result
    }
}
