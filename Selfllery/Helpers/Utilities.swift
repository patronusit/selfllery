//
//  Utilities.swift
//  Oras
//
//  Created by Ivan Zagorulko on 21.07.16.
//  Copyright © 2016 Patronus IT. All rights reserved.
//

import UIKit

final class Utilities {
    static let sharedInstance = Utilities()
    
    func documentDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return paths as NSString
    }
    
    func fixOrientation(_ img:UIImage) -> UIImage {
        
        if (img.imageOrientation == UIImageOrientation.up) {
            return img;
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        return normalizedImage;
        
    }
    
    func versionString() -> String {
        let bundleId = Bundle.main.bundleIdentifier
        let majorVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"]
        let minorVersion = Bundle.main.infoDictionary!["CFBundleVersion"]
        let versionStr = "Demo: " + "\(majorVersion!) (\(minorVersion!))"//((bundleId == "digital.norse.oras.demo") ?"Demo: " :"Dev: ") + "\(majorVersion!) (\(minorVersion!))"
        //let versionStr = "Dev: " + "\(majorVersion!) (\(minorVersion!))"//((bundleId == "digital.norse.oras.demo") ?"Demo: " :"Dev: ") + "\(majorVersion!) (\(minorVersion!))"
        return versionStr
    }
}
