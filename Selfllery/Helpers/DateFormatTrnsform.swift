//
//  DateFormatTrnsform.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 14.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import ObjectMapper

public class DateFormatTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String
    
    public init() {}
    
    var dateFormat = DateFormatter(withFormat: "yyyy-MM-dd HH:mm:ss", locale: "")
    
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormat = DateFormatter(withFormat: dateFormat, locale: "")
    }
    
    public func transformFromJSON(_ value: Any?) -> Object? {
        if let dateString = value as? String {
            return self.dateFormat.date(from: dateString)
        }
        return nil
    }
    public func transformToJSON(_ value: Date?) -> JSON? {
        if let date = value {
            return self.dateFormat.string(from: date)
        }
        return nil
    }
}
