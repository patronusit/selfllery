//
//  SLNetworkService.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 05.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import SwiftyJSON
import FBSDKLoginKit

public enum BackendError: Error {
    case Error(message: String)
    case TimeOut
    case NoInternet
}

class SLNetworkService {
    
    let lang = Bundle.main.preferredLocalizations.first!
    
    static let sharedService = SLNetworkService()
    
    let profileReqService = ProfileRequests()
    let photoReqService = PhotoRequests()
    let listingReqService = ListingRequest()
    let galleryReqService = GalleryRequests()
    let commentsReqService = CommentsRequests()
    let charityReqService = CharityRequests()
    
    let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
    
    let currentUser: Property<SLUser?> = Property(nil)
    let userToken: Property<String> = Property("")
    
    func checkBackendError(response: DataResponse<Any>?) -> BackendError? {
        switch response!.result {
        case .success(let value):
            let json = JSON(value)
            if let status = json["status"].bool, status == false {
                let message = json["message"].string
                print("🚫🚫🚫:", message)
                return .Error(message: message!)
            } else {
                print("✅✅✅:", json)
            }
            return nil
        case .failure(let error):
            if let err = error as? URLError, err.code == .notConnectedToInternet {
                print("🚫🚫🚫: NoInternet")
                return .NoInternet
            }
            if let err = error as? URLError, err.code == .timedOut {
                print("🚫🚫🚫: TimeOut")
                return .TimeOut
            }
            return .Error(message: error.localizedDescription)
        default:
            return nil
        }
        return nil
    }

// MARK: ProfileRequests
    func registrate(email: String, password: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userRegister(email: email, password: password, language: self.lang).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func auth(authType: String, email: String, password: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userAuth(authType: authType, email: email, password: password, language: self.lang).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func validateUser(userToken: String, authorId: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.validateUserToken(userToken: userToken, authorId: authorId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func updateUserProfile(nameFirst: String, nameLast: String, email: String, countryId:Int, gender: String, birthdayDate: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.updateUserProfile(userToken: self.userToken.get, nameFirst: nameFirst, nameLast: nameLast, email: email, language:  self.lang, countryId: countryId, gender: gender, birthdayDate: birthdayDate).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }

    }
    
    func userProfile(authorId: Int = 0) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.getuUserProfile(userToken: self.userToken.get, authorId: authorId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
        
    }

    func uploadUserAvatar(image: UIImage) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            self.profileReqService.setUserAvatar(userToken: self.userToken.get, avatarImage: image, encodingComplection: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let error = self.checkBackendError(response: response) {
                            subscriber.onError(error)
                        } else {
                            subscriber.onNext(response.result.value as AnyObject)
                            subscriber.onCompleted()
                        }
                    }
                case .failure(let encodingError):
                    subscriber.onError(NSError())
                }
            })
            return Disposables.create {
            }
        }
    }
    
    func uploadUserBackground(image: UIImage) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            self.profileReqService.setUserBackground(userToken: self.userToken.get, backgroundImage: image, encodingComplection: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let error = self.checkBackendError(response: response) {
                            subscriber.onError(error)
                        } else {
                            subscriber.onNext(response.result.value as AnyObject)
                            subscriber.onCompleted()
                        }
                    }
                case .failure(let encodingError):
                    subscriber.onError(NSError())
                }
            })
            return Disposables.create {
            }
        }
    }
    
    func userFollow(authorId: Int) -> Observable<AnyObject>  {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userFollow(userToken: self.userToken.get, authorId: authorId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func userFollowing(authorId: Int, page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userFollowing(userToken: self.userToken.get, authorId: authorId, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }

    func userFollowers(authorId: Int, page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userFollowers(userToken: self.userToken.get, authorId: authorId, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func userDeleteAvatar() -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userDeleteAvatar(userToken: self.userToken.get).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func userDeleteBackground() -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userDeleteBackground(userToken: self.userToken.get).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func userIgnore(ignorableUserId: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.ignoreUser(userToken: self.userToken.get, ignorableUserId: ignorableUserId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    // MARK: PhotoRequests 
    func  photoShow(photoId: Int, source: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.photoShow(userToken: self.userToken.get, photoId: photoId, source: source).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func photoUpload(image: UIImage) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            self.photoReqService.photoUpload(userToken: self.userToken.get, image: image, encodingComplection: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let error = self.checkBackendError(response: response) {
                            subscriber.onError(error)
                        } else {
                            subscriber.onNext(response.result.value as AnyObject)
                            subscriber.onCompleted()
                        }
                    }
                case .failure(let encodingError):
                    subscriber.onError(NSError())
                }
            })
            return Disposables.create {
            }
        }
    }
    
    func photoApprove(title: String, description: String, tags: [String], categoryId: Int, photoId: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.photoApprove(userToken: self.userToken.get, title: title, description: description, tags: tags, categoryId: categoryId, photoId: photoId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func photoComments(photoId: Int, page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.photoComments(userToken: self.userToken.get, photoId: photoId, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func photoCategory(categoryId: Int, page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.photoCategory(userToken: self.userToken.get, categoryId: categoryId, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func photoEditorsChoice(page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.photoEditorsChoice(userToken: self.userToken.get, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func photoPopular(page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.photoPopular(userToken: self.userToken.get, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func photoLike(photoId: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.photoLike(userToken: self.userToken.get, photoId: photoId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    // MARK: ListingRequest
    func feedListing(page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.listingReqService.feedListing(userToken: self.userToken.get, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func categoryListing()  -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.listingReqService.categoryListing(userToken: self.userToken.get).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func photoListing(page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.listingReqService.photoListing(userToken: self.userToken.get, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    //MARK: CharityRequests
    func charityListing() -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.charityReqService.charityListing(userToken: self.userToken.get).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getUserSettings() -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.charityReqService.getUserSettings(userToken: self.userToken.get).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func setUserSettings(percent: String, settings: [[String: Any]]) -> Observable<AnyObject> {
        var parameters :Parameters = [:]
        var i = 0
        for setting in settings {
            let categoryId = setting["category_id"] as! Int
            let categoryState = setting["category_enabled"] as! Bool
            parameters["\(i)"] = ["charity_category_id" : "\(categoryId)",
                                  "enabled" : categoryState]
            i += 1
        }
        return Observable.create { (subscriber) -> Disposable in
            let request = self.charityReqService.setUserSettings(userToken: self.userToken.get, percent: percent, settings: parameters).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    //MARK: GalleryRequests
    func galleryUser(page: Int, authorId: Int = 0) ->  Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.galleryReqService.galleryUser(userToken: self.userToken.get, page: page, authorId: authorId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func galleryComments(galleryId: Int, page: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.galleryReqService.galleryComments(userToken: self.userToken.get, galleryId: galleryId, page: page).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    //MARK: CommentsRequests
    func commentsPost(commentText:String, objectId: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.commentsReqService.commentsPost(userToken: self.userToken.get, commentText: commentText, objectId: objectId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func commentsReply(commentText:String, commentType:String, objectId:Int, parentId:Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.commentsReqService.commentsReply(userToken: self.userToken.get, commentText: commentText, commentType: commentType, objectId: objectId, parentId: parentId).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func commentsDelete(commentText:String, commentType:String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.commentsReqService.commentsDelete(userToken: self.userToken.get, commentText: commentText, commentType: commentType).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func commentsEdit(commentId: Int, commentText: String, commentType: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.commentsReqService.commentsEdit(userToken: self.userToken.get, commentId: commentId, commentText: commentText, commentType: commentType).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func commentsLike(commentId: Int, commentType: Int, isLike: Bool) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.commentsReqService.commentsLike(userToken: self.userToken.get, commentId: commentId, commentType: commentType, isLike: isLike).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func commentsIgnore(commentId: Int, commentType: Int) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.commentsReqService.commentsIgnore(userToken: self.userToken.get, commentId: commentId, commentType: commentType).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getFBAuth() -> Observable<AnyObject> {
        return Observable<AnyObject>.create({ (observer) -> Disposable in
            self.fbLoginManager.logIn(withReadPermissions:["email"], from: nil, handler: { (result , error) in
                if let accessToken = result?.token?.tokenString {
                    observer.onNext(accessToken as AnyObject)
                    observer.onCompleted()
                } else {
                    //observer.onError(.BackendError.Error(NSLocalizedString("facebook_token_fetch_error", comment: "")))
                }
            })
            return Disposables.create()
        }).flatMap({ (accessToken) -> Observable<AnyObject> in
            return self.authBySocial(accessToken: (accessToken ) as! String)
        }).subscribeOn(MainScheduler.instance)
    }
    
    func getGoogleAuth(accessToken: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userAuthBySocial(authType: "google", socialToken: accessToken, language: self.lang).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    private func authBySocial(accessToken: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.userAuthBySocial(authType: "facebook", socialToken: accessToken, language: self.lang).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func countriesListing() -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.profileReqService.countriesListing(userToken: self.userToken.get).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func complaintcComments(commentId: Int, complaintCommentText: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.commentsReqService.complaintComments(userToken: self.userToken.get, commentId: commentId, complaintCommentText: complaintCommentText).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func complaintcPhoto(photoId: Int, complaintPhotoText: String) -> Observable<AnyObject> {
        return Observable.create { (subscriber) -> Disposable in
            let request = self.photoReqService.complaintPhoto(userToken: self.userToken.get, photoId: photoId, complaintPhotoText: complaintPhotoText).responseJSON(completionHandler: { (response) in
                if let error = self.checkBackendError(response: response) {
                    subscriber.onError(error)
                } else {
                    switch response.result {
                    case .success(let value):
                        subscriber.onNext(value as AnyObject)
                        subscriber.onCompleted()
                    case .failure(let error):
                        subscriber.onError(error)
                    }
                }
            })
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
}
