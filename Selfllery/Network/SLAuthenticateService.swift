//
//  SLAuthenticateService.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 02.03.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

class SLAuthenticateService {
    static let sharedInstanse = SLAuthenticateService()
    let userDefaults = UserDefaults.standard
    
    let service = SLNetworkService.sharedService
    let reposytoryManager = SLRepository.sharedRepository
    
    func authenticate(_ login: String, password: String) -> Observable<AnyObject> {
        return service.auth(authType: "normal", email: login, password: password).do(onNext: { [unowned self] result in
            self.saveUserInformation(result: result)
        })
    }
    
    func authenticateByFacebook() -> Observable<AnyObject> {
        return service.getFBAuth().do(onNext: { [unowned self] result in
            self.saveUserInformation(result: result)
        })
    }
    
    func authenticateByGoogle(accessToken: String) -> Observable<AnyObject> {
        return service.getGoogleAuth(accessToken: accessToken).do(onNext: { [unowned self] result in
            self.saveUserInformation(result: result)
        })
    }
    
    func invalidateAuthenticationCredentials() {
        self.userDefaults.setValue(nil, forKey: "application_token")
        self.userDefaults.setValue(nil, forKey: "application_user_id")
        self.userDefaults.synchronize()
    }
    
    func isValidAuthorization() -> Observable<Bool> {
        if let userToken = self.userDefaults.value(forKey: "application_token") as? String, let userId = self.userDefaults.value(forKey: "application_user_id") as? Int {
            return self.service.validateUser(userToken: userToken, authorId: userId).flatMap({ success -> Observable<Bool> in
                guard let successJson = success as? AnyObject else { return  Observable.just(false) }
                let json = JSON(successJson)
                let result = json["status"].boolValue
                if result == true {
                    self.reposytoryManager.currentUserID.set(value: userId)
                    self.service.userToken.set(value: userToken)
                    self.service.currentUser.set(value: self.reposytoryManager.getUser())
                }
                return  Observable.just(result)
           })
        } else {
            return  Observable.just(false)
        }
    }
    
    func saveUserInformation(result: AnyObject) {
        if let userData = result.value(forKeyPath: "data") as? AnyObject {
            let json = JSON(userData)
            let token = json["user_token"].stringValue
            let userID = json["author_id"].intValue
            self.userDefaults.setValue(token, forKey: "application_token")
            self.userDefaults.setValue(userID, forKey: "application_user_id")
            self.userDefaults.synchronize()
            self.reposytoryManager.save(userData: result)
        }
    }
}
