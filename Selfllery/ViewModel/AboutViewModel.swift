//
//  AboutViewModel.swift
//  Selfllery
//
//  Created by victor on 28.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation

import UIKit

class AboutViewModel: ViewModel {
    let menuCommand = Command()
    
    let FAQButtonCommand = Command()
    let privacyButtonCommand = Command()
    let termsButtonCommand = Command()
}
