//
//  UploadDescriptionViewModel.swift
//  Selfllery
//
//  Created by Victor on 2/21/18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import RxSwift

class UploadDescriptionViewModel: ViewModel {
    let backCommand = Command()
    //let uploadPhotoCommand = Command()
    let addPhotoCommand = Command()
    
    let titleProperty:Property<String> = Property("")
    let descriptionProperty:Property<String> = Property("")
    let tagsProperty:Property<String> = Property("")
    let tagsArrayProperty:Property<[String]> = Property([])
    let photoImage:Property<UIImage?> = Property(nil)
    
    let photosCategory: Property<[SLCategory]> = Property([])
    let selectedCategoryId: Property<Int> = Property(0)
    
    let netSer = SLNetworkService.sharedService
    let repService = SLRepository.sharedRepository
    
    let photoID: Property<Int> = Property(0)
    
    override init() {
        super.init()
        
        self.photosCategory.set(value: self.repService.getCategories())
    }
    
//    lazy var loadCategoryesCommand: AsyncCommand<[SLCategory]> = AsyncCommand {
//        return self.repService.loadPhotoCategory().do(onNext: { [unowned self] result in
//            self.photosCategory.set(value: result)
//        })
//    }

    lazy var uploadPhotoCommand:AsyncCommand<Bool> = AsyncCommand {
        if self.photoImage.get != nil {
            return self.netSer.photoUpload(image: self.photoImage.get!).flatMap({ success -> Observable<Bool> in
                guard let successJson = success as? AnyObject else { return  Observable.just(false) }
                if let res = successJson.value(forKeyPath:"photo_id") {
                    self.photoID.set(value: res as! Int)
                }
                return Observable.just(true)
            })
        } else {
            return Observable.just(false)
        }
    }
    
    lazy var approvePhotoCommand: AsyncCommand<AnyObject> = AsyncCommand {
        return self.netSer.photoApprove(title: self.titleProperty.get,
                                        description: self.descriptionProperty.get,
                                        tags: self.tagsArrayProperty.get, categoryId: self.selectedCategoryId.get, photoId: self.photoID.get)
    }
    
}
