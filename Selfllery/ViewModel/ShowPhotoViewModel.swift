//
//  ShowPhotoViewModel.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 27.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import RxSwift

class ShowPhotoViewModel: ViewModel {
    
    let closeCommand = Command()
    
    var preseentPhoto: Property<UIImage?> = Property(nil)
}
