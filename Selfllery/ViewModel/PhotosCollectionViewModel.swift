//
//  PhotosCollectionViewModel.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 15.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class PhotosCollectionViewModel: ViewModel {
    let backCommand = Command()
    
    let repService = SLRepository.sharedRepository
    
    let galeryPhotos: Property<[SLPhoto]> = Property([])
    let selectPhoto: Property<SLPhoto?> = Property(nil)
    let photosGaleryName = Property("")
    let photosGaleryId = Property(0)
    let loadGaleryPage = Property(0)
    
    lazy var loadGaleryPhotosCommand: AsyncCommand<[SLPhoto]> = AsyncCommand {
        return self.repService.loadGaleryPhotos(categoryId: self.photosGaleryId.get, page: self.loadGaleryPage.get).do(onNext: { [unowned self] result in
            self.galeryPhotos.set(value: result)
        })
    }
}
