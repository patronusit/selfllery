//
//  EditProfileViewModel.swift
//  Selfllery
//
//  Created by victor on 11.02.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class EditProfileViewModel: ViewModel {
    let menuCommand = Command()
    let backCommand = Command()
    
    var ownerUser: Property<SLUser?> = Property(nil)
    let netSer = SLNetworkService.sharedService
    let reposytoryManager = SLRepository.sharedRepository
    
    let changeAvatarImageCommand = Command()
    let changeBackgroundImageCommand = Command()
    
    let avatarImage:Property<UIImage?> = Property(nil)
    let backgroundImage:Property<UIImage?> = Property(nil)
    
    let nameFirstProperty:Property<String> = Property("")
    let nameLastProperty:Property<String> = Property("")
    let emailProperty:Property<String> = Property("")
    
    let countryIdProperty:Property<Int> = Property(1)
    let genderProperty:Property<String> = Property("Male")
    let birthdayDateProperty:Property<String> = Property("")
    let countiesArray: Property<[SLCountry]> = Property([])
    
    let isOwnProfile = Property(false)

    override init() {
        super.init()
        self.netSer.currentUser.change.subscribe(onNext: { [unowned self] curUser in
            self.ownerUser.set(value: curUser)
        })
        
        self.countiesArray.set(value: self.reposytoryManager.getCountries())
    }
    
    lazy var confirmChangesCommand: AsyncCommand<Bool> = AsyncCommand {
        return self.reposytoryManager.updateUserProfile(nameFirst: self.nameFirstProperty.get,
                                             nameLast: self.nameLastProperty.get,
                                             email: self.emailProperty.get,
                                             countryId: self.countryIdProperty.get,
                                             gender: self.genderProperty.get,
                                             birthdayDate: self.birthdayDateProperty.get)
    }
    
    lazy var uploadAvatarImageCommand: AsyncCommand<Bool> = AsyncCommand {
        return  self.reposytoryManager.uploadAvatar(avatarImage: self.avatarImage.get!)
    }
    
    lazy var uploadBackgroundImageCommand:AsyncCommand<Bool> = AsyncCommand {
        return  self.reposytoryManager.uploadBackground(backgroundImage: self.backgroundImage.get!)
    }
}
