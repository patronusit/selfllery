//
//  MainViewModel.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 05.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class MainViewModel: ViewModel {
    
    let menuCommand = Command()
    
    let netSer = SLNetworkService.sharedService
    let repService = SLRepository.sharedRepository
    
    var user: Property<SLUser?> = Property(nil)
    var selectMenu: Property<IndexPath?> = Property(nil)
    
    override init() {
        super.init()
        
        self.user.set(value: repService.getUser())
        
        self.netSer.currentUser.change.subscribe(onNext: { [unowned self] curUser in
            if curUser != nil {
                self.user.set(value: curUser)
            }
        })
    }
    
    lazy var countriesListingCommand:AsyncCommand<Bool> = AsyncCommand {
        return self.repService.loadCountries()
    }
    
    lazy var charityListingCommand:AsyncCommand<Bool> = AsyncCommand {
        return self.repService.loadCharities()
    }
}
