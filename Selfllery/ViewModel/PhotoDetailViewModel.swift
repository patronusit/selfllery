//
//  PhotoDetailViewModel.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 19.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import RxSwift

class PhotoDetailViewModel: ViewModel {
    let backCommand = Command()
    let showUserProfileCommand = Command()
    
    let repService = SLRepository.sharedRepository
    let netSer = SLNetworkService.sharedService

    let photoDetail: Property<SLPhoto?> = Property(nil)
    let photoAuthorName: Property<String> = Property("")
    let photoAuthorAvatarUrl: Property<String> = Property("")
    let photoDescription: Property<String> = Property("")
    let photoComments: Property<[SLComment]> = Property([])
    
    let complaintCommentId = Property(0)
    let complaintCommentText = Property("")
    
    let complaintPhotoId = Property(0)
    let complaintPhotoText = Property("")
    
    let localPhoto: Property<SLPhoto?> = Property(nil)
    let localPhotoId = Property(0)
    let loadCommentsPage = Property(0)
    let isLikedPhoto = Property(false)
    
    override init() {
        super.init()
        
        self.photoDetail.change.subscribe(onNext: { [unowned self] photo in
            if photo != nil {
                self.localPhoto.set(value: photo)
                self.localPhotoId.set(value: photo!.photoID)
                self.isLikedPhoto.set(value: photo!.isLiked)
            }
        })
    }
    
    lazy var likePhotoCommand: AsyncCommand<Bool> = AsyncCommand {
        if self.isLikedPhoto.get {
            return Observable.just(false)
        }
        return self.repService.likePhoto(photoId: self.localPhotoId.get)
    }
    
    lazy var loadPhotoCommentsCommand: AsyncCommand<[SLComment]> = AsyncCommand {
        return self.repService.loadPhotoComments(photoId: self.localPhotoId.get, page: self.loadCommentsPage.get).do(onNext: { [unowned self] result in
            self.photoComments.set(value: result)
        })
    }
    
    lazy var complaintCommentsCommand: AsyncCommand<AnyObject> = AsyncCommand {
        return self.netSer.complaintcComments(commentId:self.complaintCommentId.get, complaintCommentText: self.complaintCommentText.get)
    }
    
    lazy var complaintPhotoCommand:AsyncCommand<AnyObject> = AsyncCommand {
         return self.netSer.complaintcPhoto(photoId: self.complaintPhotoId.get, complaintPhotoText: self.complaintPhotoText.get)
    }
}
