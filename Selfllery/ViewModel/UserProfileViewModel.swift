//
//  UserProfileViewModel.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 20.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import Foundation
import Realm

class UserProfileViewModel: ViewModel {
    let backCommand = Command()
    let editProfileCommand = Command()
    let ignoreUserCommand = Command()
    
    let netSer = SLNetworkService.sharedService
    let repService = SLRepository.sharedRepository
    
    let userProfileId: Property<Int> = Property(0)
    let currentUser: Property<SLUser?> = Property(nil)
    let userPhotos: Property<[SLPhoto]> = Property([])
    let selectPhoto: Property<SLPhoto?> = Property(nil)
    let userFollowers: Property<[SLUser]> = Property([])
    let userFollowing: Property<[SLUser]> = Property([])
    let selectUser: Property<SLUser?> = Property(nil)
    let userComments: Property<[SLComment]> = Property([])
    let userSettings: Property<[SLSetting]> = Property([])
    
    let cSettings: Property<[[String: Any]]> = Property([])
    
    let userCharityPercent: Property<String> = Property("0%")
    
    let isOwnProfile = Property(false)
    
    let activeTabNumber = Property(0)
    let loadGaleryPage = Property(0)
    let loadCommentsPage = Property(0)
    let loadFollowersPage = Property(0)
    let loadFollowingPage = Property(0)
    
    override init() {
        super.init()
        self.netSer.currentUser.change.subscribe(onNext: { [unowned self] curUser in
            self.currentUser.set(value: curUser)
            if self.isOwnProfile.get {
                self.userCharityPercent.set(value: "\(curUser!.totalPercent)%")
                self.cSettings.set(value: curUser!.getSettings())
            }
        })
    }
    
    lazy var followUserCommand: AsyncCommand<Bool> = AsyncCommand {
        return self.repService.followUser(userId: self.userProfileId.get)
    }
    
    lazy var hideUserCommand: AsyncCommand<AnyObject> = AsyncCommand {
        return self.netSer.userIgnore(ignorableUserId: self.userProfileId.get)
    }
    
    lazy var charityUserCommand: AsyncCommand<Bool> = AsyncCommand {
        var percentStr = String(self.userCharityPercent.get.characters.dropLast())
        let settings: [[String: Any]] = self.cSettings.get
        return self.repService.charityUser(userSettings: settings, totalPercent: percentStr)
    }
    
    lazy var loadUserProfileCommand: AsyncCommand<SLUser?> = AsyncCommand {
        return self.repService.loadUserProfile(userId: self.userProfileId.get).do(onNext: { [unowned self] result in
            self.currentUser.set(value: result)
        })
    }
    
    lazy var loadUserPhotoCommand: AsyncCommand<[SLPhoto]> = AsyncCommand {
        return self.repService.loadUserPhoto(page: self.loadGaleryPage.get, userId: self.userProfileId.get).do(onNext: { [unowned self] result in
            self.userPhotos.set(value: result)
        })
    }
    
    lazy var loadUserCommentsCommand: AsyncCommand<[SLComment]> = AsyncCommand {
        return self.repService.loadUserComments(userId: self.userProfileId.get, page: self.loadCommentsPage.get).do(onNext: { [unowned self] result in
            self.userComments.set(value: result)
        })
    }
    
    lazy var loadUserFollowersCommand: AsyncCommand<[SLUser]> = AsyncCommand {
        return self.repService.loadUserFollowers(page: self.loadFollowersPage.get, userId: self.userProfileId.get).do(onNext: { [unowned self] result in
            self.userFollowers.set(value: result)
        })
    }
    
    lazy var loadUserFollowingCommand: AsyncCommand<[SLUser]> = AsyncCommand {
        return self.repService.loadUserFollowing(page: self.loadFollowingPage.get, userId: self.userProfileId.get).do(onNext: { [unowned self] result in
            self.userFollowing.set(value: result)
        })
    }
    
    lazy var loadUserSettingsCommand: AsyncCommand<[SLSetting]> = AsyncCommand {
        return self.repService.loadUserSettings().do(onNext: { [unowned self] result in
            self.userSettings.set(value: result)
        })
    }
    
    func changeSetting(setting: SLSetting) {
        self.repService.checkSetting(setting: setting)
    }
}
