//
//  PhotosViewModel.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 12.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class PhotosViewModel: ViewModel {
    let menuCommand = Command()
    
    let netSer = SLNetworkService.sharedService
    let repService = SLRepository.sharedRepository
    
    let activeTabNumber = Property(0)
    
    let popularPhotos: Property<[SLPhoto]> = Property([])
    let editorChoicePhotos: Property<[SLPhoto]> = Property([])
    let photosCategory: Property<[SLCategory]> = Property([])
    let selectCategory: Property<SLCategory?> = Property(nil)
    let selectPhoto: Property<SLPhoto?> = Property(nil)
    let loadPopularPage = Property(0)
    let loadEditorChoicePage = Property(0)
    
    override init() {
        super.init()
        
        self.photosCategory.set(value: self.repService.getCategories())
    }
    
    lazy var loadPopularPhotosCommand: AsyncCommand<[SLPhoto]> = AsyncCommand {
        return self.repService.loadPopularPhotos(page: self.loadPopularPage.get).do(onNext: { [unowned self] result in
            self.popularPhotos.set(value: result)
        })
    }
    
    lazy var loadEditorChoicePhotosCommand: AsyncCommand<[SLPhoto]> = AsyncCommand {
        return self.repService.loadEditorChoicePhotos(page: self.loadPopularPage.get).do(onNext: { [unowned self] result in
            self.editorChoicePhotos.set(value: result)
        })
    }
    
//    lazy var loaPhotosCategoryCommand: AsyncCommand<[SLCategory]> = AsyncCommand {
//        return self.repService.loadPhotoCategory().do(onNext: { [unowned self] result in
//            self.photosCategory.set(value: result)
//        })
//    }
}
