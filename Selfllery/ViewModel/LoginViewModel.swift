//
//  LoginViewModel.swift
//  Selfllery
//
//  Created by Ivan Zagorulko on 05.02.18.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

//email: "test5@gmail.com"
//password: "qwe123qwe"

import UIKit
import RxSwift
class LoginViewModel: ViewModel {
    
    let privacyButtonCommand = Command()
    let termsButtonCommand = Command()
    

//    let email = Property("test4@gmail.com")
//    let password = Property("qwe123qwe")
        
//    let email = Property("test5@gmail.com")
//    let password = Property("qwe123qwe")

    let email = Property("")
    let password = Property("")
    let repassord = Property("")
    let googleToken = Property("")
        
    let netSer = SLNetworkService.sharedService
    let reposytoryManager = SLRepository.sharedRepository
    
    let authenticator = SLAuthenticateService.sharedInstanse
    
    lazy var registerCommand: AsyncCommand<AnyObject> = AsyncCommand(canExecute: Observable.combineLatest(self.email.change ,self.password.change, self.repassord.change) {
        !$0.isEmpty && !$1.isEmpty && !$2.isEmpty && (!$1.isEmpty == !$2.isEmpty)
        })
    {
        return self.netSer.registrate(email:  self.email.get, password: self.password.get).do(onNext: { [unowned self] result in
            self.reposytoryManager.save(userData: result)
            self.reposytoryManager.loadCountriesListing()
            self.reposytoryManager.loadCharityListing()
            self.reposytoryManager.loadCategoryOfPhoto()
        })
    }
 
    lazy var authenticateCommand: AsyncCommand<AnyObject> = AsyncCommand(canExecute: Observable.combineLatest(self.email.change, self.password.change) {
        !$0.isEmpty && !$1.isEmpty
    }) {
        return self.authenticator.authenticate(self.email.get, password: self.password.get).do(onNext: { [unowned self] result in
            self.reposytoryManager.loadCountriesListing()
            self.reposytoryManager.loadCharityListing()
            self.reposytoryManager.loadCategoryOfPhoto()
        })
    }
    
    lazy var authenticateByFBCommand: AsyncCommand<AnyObject> = AsyncCommand { [unowned self] in
        return self.authenticator.authenticateByFacebook().do(onNext: { [unowned self] result in
            self.reposytoryManager.loadCountriesListing()
            self.reposytoryManager.loadCharityListing()
            self.reposytoryManager.loadCategoryOfPhoto()
        })
    }
    
    lazy var googleButtonCommand: AsyncCommand<AnyObject> = AsyncCommand { [unowned self] in
        return self.authenticator.authenticateByGoogle(accessToken: self.googleToken.value).do(onNext: { [unowned self] result in
            self.reposytoryManager.loadCountriesListing()
            self.reposytoryManager.loadCharityListing()
            self.reposytoryManager.loadCategoryOfPhoto()
        })
    }
    
    lazy var validUserNameCommand: AsyncCommand<Bool> = AsyncCommand { [unowned self] in 
        return self.authenticator.isValidAuthorization().do(onNext: { [unowned self] result in
            self.reposytoryManager.loadCountriesListing()
            self.reposytoryManager.loadCharityListing()
            self.reposytoryManager.loadCategoryOfPhoto()
        })
    }
}

